<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>L=A=B=O=R=A=T=O=R=I=O D=E P=R=A=T=I=C=A=S T=E=X=T=U=A=I=S E=X=P=E=R=I=M=E=N=T=A=I=S</title>
        <meta name="description" content="L=A=B=O=R=A=T=O=R=I=O D=E P=R=A=T=I=C=A=S T=E=X=T=U=A=I=S E=X=P=E=R=I=M=E=N=T=A=I=S">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/foundation.css">
        <link rel="stylesheet" href="css/app.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

        <?php
          //ini_set('display_errors', 1);
          //ini_set('display_startup_errors', 1);
          //error_reporting(E_ALL);
        ?>

        <div class="row header">
          <div id="logo" class="columns small-12 medium-6 large-4">
            <a href="index.php">
              <span class="first">L</span><span>A</span><span>B</span><span>O</span><span>R</span><span>A</span><span>T</span><span>O</span><span>R</span><span>I</span><span>O</span><br>
              <span class="first">D</span><span class="last">E</span><br>
              <span class="first">P</span><span>R</span><span>A</span><span>T</span><span>I</span><span>C</span><span>A</span><span>S</span><br>
              <span class="first">T</span><span>E</span><span>X</span><span>T</span><span>U</span><span>A</span><span>I</span><span>S</span><br>
              <span class="first">E</span><span>X</span><span>P</span><span>E</span><span>R</span><span>I</span><span>M</span><span>E</span><span>N</span><span>T</span><span>A</span><span>I</span><span>S</span></a>
          </div>
          <ul class="mymenu columns small-6 medium-2 large-2">
            <li<?php if( preg_match('/^index-[1-8]/', $_GET["p"]) ) { echo ' class="active"'; }?>><span>Sessões</span></li>
            <li<?php if( $_GET["p"] == "autores") { echo ' class="active"'; }?>><a href="?p=autores">Autores</a></li>
            <li<?php if( $_GET["p"] == "index-9-projetos") { echo ' class="active"'; }?>><a href="?p=index-9-projetos">Projetos finais</a></li>
            <li<?php if( $_GET["p"] == "noticias") { echo ' class="active"'; }?>><a href="?p=noticias">Notícias</a></li>
            <li<?php if( $_GET["p"] == "colophon") { echo ' class="active"'; }?>><a href="?p=colophon">Colophon</a></li>
          </ul>
          <ul class="mysubmenu columns small-6 medium-4 large-6">
            <li<?php if( $_GET["p"] == "index-1-timing") { echo ' class="active"'; }?>><a href="?p=index-1-timing">Timing</a></li>
            <li<?php if( $_GET["p"] == "index-2-entrevista") { echo ' class="active"'; }?>><a href="?p=index-2-entrevista">Entrevista</a></li>
            <li<?php if( $_GET["p"] == "index-3-erasure") { echo ' class="active"'; }?>><a href="?p=index-3-erasure">Erasure</a></li>
            <li<?php if( $_GET["p"] == "index-4-cutup") { echo ' class="active"'; }?>><a href="?p=index-4-cutup">Cut-up</a></li>
            <li<?php if( $_GET["p"] == "index-5-oulipo") { echo ' class="active"'; }?>><a href="?p=index-5-oulipo">Oulipo</a></li>
            <li<?php if( $_GET["p"] == "index-6-flarf") { echo ' class="active"'; }?>><a href="?p=index-6-flarf">Flarf</a></li>
            <li<?php if( $_GET["p"] == "index-7-conceptual") { echo ' class="active"'; }?>><a href="?p=index-7-conceptual">Conceptual writing</a></li>
            <li<?php if( $_GET["p"] == "index-8-conclusao") { echo ' class="active"'; }?>><a href="?p=index-8-conclusao">Conclusão</a></li>
          </ul>
        </div>

        <section class="main" id="<?php echo ($_GET["p"]); ?>">
          <div class="row"><div class="columns small-12 medium-8 medium-offset-2">
            <?php include 'render-text.php'; ?>

          </div></div>
        </section>

        <!--
        <footer class="footer">
          <div class="row"><div class="columns small-12">
            <ul>
              <li>Aqui está a footer</li>
              <li>e podemos colocar aqui coisas</li>
            </ul>
          </div></div>
        </footer>
        -->

        <script src="js/vendor/jquery-1.11.2.min.js"></script>
        <script src="js/vendor/foundation.min.js"></script>
        <script src="js/plugins.js"></script>
    </body>
</html>
