
// índice: lista dos ficheiros
$page_files_list = scandir('textos');
// retirar "." e ".."
unset($page_files_list[0]);
unset($page_files_list[1]);
$file_to_load = $page_files_list[array_rand($page_files_list)];

echo '<ul>';
foreach ($page_files_list as $fn) {
  $slug = str_replace(".md", "", $fn);
  echo '<li><a href="?p=' . $slug . '">' . $slug . '</a></li>' ;
}
echo '</ul>';

