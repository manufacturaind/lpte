      L=A=B=O=R=A=T=O=R=I=O
              D=E
         P=R=A=T=I=C=A=S
         T=E=X=T=U=A=I=S
    E=X=P=E=R=I=M=E=N=T=A=I=S
    
Arquivo e documentação da 1ª edição do 
[Laboratório de Práticas Textuais Experimentais](http://laboratoriodepraticastextuaisexperimentais.com).  
Concebido e conduzido por bruno neiva, contou ainda com a participação 
e apoio do Professor Pedro Eiras.
