<?php
require_once 'Michelf/MarkdownExtra.inc.php';

if ( array_key_exists("p", $_GET ) ) {
  $file_to_load = $_GET["p"] . ".md";
} else {
  $file_to_load = "intro.md";
}

// markdown2html
$page_contents_markdown = file_get_contents('textos/' . $file_to_load);
$md = new \Michelf\MarkdownExtra();
$page_contents_html = $md->defaultTransform($page_contents_markdown);
echo $page_contents_html;

?>
