## Colophon

### Conceção, formação e edição
##### bruno neiva

### Docente responsável pela formação
##### Pedro Eiras

### Instituição de ensino
##### Faculdade de Letras da Universidade do Porto (FLUP)

### Web design
##### Manufactura Independente

### Formandos
##### Ana Isabel Carvalho, Gabriel Innocentini, Maria Stella, Ricardo Lafuente e Soares Vieira

### Data
##### 2016/02/18 – 2016/04/28
