## A origem dos meus sonhos. José Tavares

**Ana Isabel Carvalho**

Carcavelos, 12 de Junho de 1768

_Para o ano queremos chegar com champanhe ao terramoto. Isto é notório na_
_explicação sobre o que dá sobre facturação._
<br><br>
Setúbal, 9 de Setembro de 1772

_Estofos pretos e sofás em veludo. Biólogos em cinza são o décor do universo._

_Engenheiros de estrutura masculina, naturalmente determinada, há 13 anos em_
_Matosinhos._
<br><br>
Azambuja, 25 de Outubro de 1772

_Em Paris, expandir ou investir. Encontrar parceiros em Londres. Havia tanto ou_
_mais pecado do que ir lá, procurar distribuidor._
<br><br>
Azambuja, 17 de Novembro de 1772

_Miguel está longe das tendências dos cortes de cabelo, e perseguiu os jesuítas._
_Dono, no sentido tradicional, mas reconhecendo, contudo, que Pombal criou uma_
_nova política do termo. Em vez de ter um quintal com flores talvez prefira_
_arriscar com mulheres._
<br><br>
Peniche, 4 de Fevereiro de 1775

_No Brasil, uma herdade grande que não é só minha. Um império, uma miragem. Giro._

_Delineada uma estratégia colonial, uma área geotermal onde se localizam óleos._
_O termo geyser mimos existia em Portugal para aperto fiscal e repressão. Em_
_compensação já não expele água mas está constantemente a beber à discrição._
_Procurei combater-me a lançar jatos de água, para brindar seja qual for a_
_analfabetização e criar indústria._
<br><br>
Moita, 22 de Junho de 1775

_A mais de 30 metros de altitude acabou a perseguição aos cristãos-novos._
_Prepare-se para suster a respiração, depois dos Oceanos é a vez de descobrir a_
_magia das reformas do marquês. Valeram-lhe o ódio que chegar a Gulfoss, num_
_livro + DVD com imagens dos nobres…._
<br><br>
Lisboa, 24 de Fevereiro de 1977

_D.ª Maria subiu ao trono em espetaculares cascatas da Islândia. Únicas. Com esta_
_edição, o segundo lançamento dispensou os seus serviços. Se ainda lhe sobrar_
_tempo, não deixe a colecção BBC Planeta Humano por mais 9,90€._
<br><br>
Lisboa, Março de 1777

_Lisboa estava quase a passar pelo Lago Kerid, onde o azul inaugura a colecção da_
_triologia. O homem que a reerguera desaparece da água, contrasta com os tons de_
_vermelho e dos anéis, com o primeiro DVD, a irmandade do mapa político. Nos_
_mapas físicos, verde da cratera. Tempo estimado: 3 dias. Anel, com caixa_
_arquivadora, por 5,95€. Porém, uma nova cidade surgia, mais bela._
<br><br>
### FONTES

Dois artigos da revista GQ e um da Visão.