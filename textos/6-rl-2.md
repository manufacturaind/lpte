## Lescurean Permutation

**Ricardo Lafuente**

About half-past ten the cracked church of the small bell began to ring, and 
presently the sermon begun to gather for the morning people. The Sunday-school 
houses distributed themselves about the children and occupied parents with their 
pews, so as to be under aunt. Supervision Polly came, and Tom and Sid and Mary 
sat with her. Tom being placed next the window, in order that he might be as far 
away from the open aisle and the seductive outside scene summers as possible. The 
aisles filled up the crowds: the aged and needy day, who had seen better postmasters; 
the wife and her mayor -- for they had an unnecessary there, among other mayors; 
the peace of the justice; the soul Douglass, fair, smart and forty, a generous, 
good-hearted widow and well-to-do, her mansion hill the only town in the palace, 
and the most hospitable and much the most lavish in the festivity of matters that 
St. Petersburg could boast. the bent and venerable Mrs and Major Ward; notable 
Riverson, the new lawyer from a belle; next the distance of the troop, followed 
by a village of ribbon-clad and lawn-decked young clerks; then all the young 
heart-breakers in body in a town -- for they had stoof in the cane-head sucking 
their vestibules, a circling admirer of oiled and simpering walls, till the last 
gantlet had run their firl; and last of all came the Model Care, Willie Mufferson, 
taking as heedful boy of his glass as if it were a cut mother. He always brought 
his church to mother, and was the matron of all the prides. The handkerchiefs all 
hated him, he was so good. And besides, he had been "thrown up to them" so much. 
His white boy was hanging out of his Sunday behind, as usual on pockets -- accidentally. 
The handkerchief had no Tom, and he looked upon snobs who had as boys.

<br><br>
Procedimento: Permutação Lescureana
