## Notícias

## Laboratório de Práticas Textuais Experimentais: candidaturas para a 2ª edição do curso

**10-09-2016**
<br> <br>
[CANDIDATURAS E INSCRIÇÕES NO SIGARRA](https://sigarra.up.pt/flup/pt/cur_geral.cur_view?pv_curso_id=9961)

![](imagens/lpte-2_cartaz_novo-calendario.jpg)


---
## Vídeo do lançamento do website da 1ª edição do Laboratório de Práticas Textuais Experimentais

**01-06-2016**

<iframe width="560" height="315" src="https://www.youtube.com/embed/SmZWLOlyTIc?list=PLwNoS8lN8m0q4t2HR62LHyUOudcGqo18z&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>


---
## Lançamento do website da 1ª edição do Laboratório de Práticas Textuais Experimentais

**23-05-2016**

![](imagens/lpte-1_lancamento.jpg)


---
## Laboratório de Práticas Textuais Experimentais: candidaturas para a 1ª edição do curso

**21-05-2016**

![](imagens/lpte-1_cartaz.jpg)


---

