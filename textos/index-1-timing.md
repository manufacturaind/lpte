## Sessão nº1: Timing

**18/02/2016**

<hr>

### Discussão

* Laboratório de Práticas Textuais Experimentais: Introdução
* Derek Beaulieu: "Please, No More Poetry"
* Charles Bernstein: "Thank you for saying thank you"
* Charles Bernstein: "What Makes a Poem a Poem"

### Procedimento

* Dicionário

Produza um mini-dicionário (uma palavra por cada letra do alfabeto) com significados alternativos. Utilize material apropriado.

### Textos produzidos

* [Comandos](http://laboratoriodepraticastextuaisexperimentais.com/?p=1-rl)
* [Dicionário](http://laboratoriodepraticastextuaisexperimentais.com/?p=1-bv)
* [Dicionário](http://laboratoriodepraticastextuaisexperimentais.com/?p=1-ms)
* [Dicionário](http://laboratoriodepraticastextuaisexperimentais.com/?p=1-pe)
* [Singular suave](http://laboratoriodepraticastextuaisexperimentais.com/?p=1-aic)

### Links e bibliografia

* BEAULIEU, Derek. (2013). [*Please, No More Poetry*](http://theamericanreader.com/bookcase/please-no-more-poetry-the-poetry-of-derek-beaulieu/). “Please, No More
Poetry”. Wilfrid Laurier University Press. Waterloo, Canadá. 1-2.
* BERNSTEIN, Charles. (2004). “What Makes a Poem a Poem”. 60-Second
Lecture. UPenn. Filadélfia, EU. [vídeo](https://www.youtube.com/watch?v=auhINfzRcyY), [transcrição](http://writing.upenn.edu/pennsound/x/Bernstein-What_Makes_a_Poem.html).
* BERNSTEIN, Charles. (2010). *Thank You for Saying Thank You*. “All the
whiskey in heaven: selected poems”. Straus, Farrar and Giroux. Nova
Iorque, EUA. 255-258. [vídeo](https://www.youtube.com/watch?v=auhINfzRcyY), [livro](https://books.google.pt/books?id=ZAkm1w2ctpoC&pg=PA7&lpg=PA7&dq=charles+bernstein+thank+you+for+saying+thank+you&source=bl&ots=kPcMJLpjcV&sig=QsqaVg4ENVp290veF0fE8QJ7Byg&hl=pt-PT&sa=X&ved=0ahUKEwjQ4LP8wf_KAhUFWhQKHX_1C-EQ6AEIUTAK#v=onepage&q=charles%20bernstein%20thank%20you%20for%20saying%20thank%20you&f=false)

### Material adicional

* [BEAULIEU, Derek](https://derekbeaulieu.wordpress.com/)
* BERNSTEIN, Charles ([EPC Author Page](http://wings.buffalo.edu/epc/authors/bernstein/))
* L=A=N=G=U=A=G=E ([Eclipse Editions Archive](http://eclipsearchive.org/projects/LANGUAGE/language.html))
