## Autores

### Formador
##### bruno neiva

Poeta e artista textual.

[http://brunoneiva.weebly.com](http://brunoneiva.weebly.com)
<br><br>

### Docente responsável pela formação
##### Pedro Eiras

Pedro Eiras é Professor de Literatura Portuguesa na FLUP, investigador, escritor; 
desde 2001, publicou livros de ensaio (entre outros, Alumiação, Constelações, 
Os Ícones de Andrei, Tentações), teatro (Um Forte Cheiro a Maçã, Uma Carta a Cassandra, 
Um Punhado de Terra), ficção (A Cura, Bach, Cartas Reencontradas de Fernando Pessoa).
<br><br>
### Formandos

##### Ana Isabel Carvalho

Designer e ativista de software e cultura livres.
<br><br>
##### Gabriel Innocentini

Nascido em São Carlos (SP), em 1987.
<br><br> 
##### Maria Stella

Nascida no Porto, para o mundo, eterna aprendiz de argonauta, viajante à descoberta do mundo e da sua leveza.
<br><br>
##### Ricardo Lafuente

Designer e hacker com um gosto particular pelos media digitais.
<br><br>

##### Soares Vieira

Já foi estudante de artes; atualmente escreve poesia e estuda um dos campos das ditas Humanidades.
