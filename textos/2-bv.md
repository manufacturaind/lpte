## Woody and Charlie, clash of egos

**Soares Vieira**

**WOODY ALLEN**:

You’ve written a lot of words.

**CHARLES BUKOWSKI**:

My poems are a little more emotional, I let myself go a little more. But
in prose I’m pretty straight.

**WOODY ALLEN:**

Do you ever think you’ll wake up one morning and not be funny?

**CHARLES BUKOWSKI:**

I’m not a great fucker.

**WOODY ALLEN:**

It’s a wonderful thing to be able to create your own world whenever you
want to. Writing is very pleasurable, very seductive, and very
therapeutic.

**CHARLES BUKOWSKI:**

When people tell me how painful it is to write I don’t understand it
because it’s just like rolling down the mountain you know. It’s freeing.

It’s enjoyable. It’s a gift and you get paid for what you want to do.

**WOODY ALLEN:**

You and I could be standing over \[William\] Shakespeare's grave.

**CHARLES BUKOWSKI:**

You can say "So-and-so is a lousy actor!" But you can't say Shakespeare
is shit. The longer something is around, snobs begin to attach
themselves to it, like suckerfish. When snobs feel something is
safe...they attach. The moment you tell them the truth, they go wild.

**WOODY ALLEN:**

He was crazy. I don’t have any of that.

**CHARLES BUKOWSKI:**

The process of being right and wrong...You know, we're monstrosities. If
we could really see this, we could love ourselves...realize how
ridiculous we are.

**WOODY ALLEN:**

I changed. I learned empathy over the years.

**CHARLES BUKOWSKI:**

Somehow in our construction, it is not meant to be. This is why I like
to see boxing matches, and why, in my younger days, I'd like to duke it
in back alleys.

I don't know a thing about it, but I believe in this "goodness" like a
tube running through our bodies.
<br><br>
### LINKS
C. BUKOWSKI:

http://www.thenervousbreakdown.com/cbukowski/2011/09/cb/

http://www.culturalweekly.com/charles-bukowski-its-humanity-that-bothers-me/

https://www.brainpickings.org/2013/09/04/charles-bukowski-on-writing/

http://bukowski.net/poems/int2.php

WOODY ALLEN:

http://www.rkpuma.com/woody.htm 

http://www.theparisreview.org/interviews/1550/the-art-of-humor-no-1-woody-allen

http://www.independent.co.uk/news/people/woody-allen-admits-he-is-too-lazy-to-make-great-films-10431756.html 
