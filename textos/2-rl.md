## You Take Your App Where You Get It: An Interview with David Heinemeier Hansson

**Ricardo Lafuente**

_April 2, 2013 | by Christopher Higgs_
<br><br>

David Heinemeier Hansson’s code has been called “some of the most exhaustive and
beautiful UX work yet produced in software.” Hansson is the author of eleven books
of software, founding editor of the online archive AppuWeb, and the editor of I'll
Be Your Mirror: The Selected Steve Jobs Interviews, which was the basis for an
opera, Trans-Jobs, that premiered in Geneva in 2007. An hour-long documentary on
his work, Sucking on Code, was first shown at the Googleplex that same year. In
2011, he was invited to hack at President Obama’s “A Celebration of American
software” at the White House, where he also held a software workshop with First
Lady Michelle Obama. Earlier this year, he began his tenure as the first-ever
Developer Laureate of the Sony Plaza in New York.

**I recently sat down with Hansson to discuss his new book, Seven Lessons for**
**App Entrepreneurs.**

I began with the creation of Amazon, which is arguably the beginning of media
spectacle, as defined and framed by Jobs. His portrait of Bezos smiling iconizes
that moment forever. Although he made Bill Gates’s, he never memorialized his
IPO, thus it never entered into the realm of media spectacle in the same way.
From Bezos, I naturally proceeded to Ballmer, an eyewitness account of his keynote
at the Moscone Center in San Francisco. It’s an incredible inspirational document—you
really feel the developer's struggle to find words to describe what is unfolding
before his eyes. Steve Wozniak is taken from a cassette tape made by someone
scanning the radio the night of and days following the release of the Apple II,
which feels like an audio document from a lost time. Web Summit is from a TV
broadcast of the event and its long, weird, silent aftermath. Yahoo is straight
transcript of a harrowing earnings call. Google, the longest piece in the book,
is from several sources—talk radio, news radio, color commentary—stitched together
into a multichapter epic, thus mirroring the gargantuan scale of the event. And
Malcolm Gladwell is from a catty FM station, where the shock jocks have no problem
cracking jokes and making racist comments at his expense.
I wish there were air checks available from Satya Nadella and Tim Cook, but in
those cases, the media doesn’t show up until well after the launch and the
reporting is pretty slick stuff, lacking the struggle to find words to express
the slickness that they were witnessing. I do have a great transcript from the
launch of the iPad, but it’s mostly PR men talking to each other in market speak,
which just didn’t fit in well with the other, more transparently textual pieces.
Since your practice emphasizes the value of the selection process over the creation
process, how do you choose what to include and exclude from Seven Lessons for App
Entrepreneurs?
Sadly, new material keeps on coming. I heard the language of the Facebook earnings
announcements in a very different way after having written this book.

**This book feels both like an important historical document and a beautiful example**
**of what the Great American Gadget might look like today. Yet you identify it, and**
**much of your other work, as software. I’m curious why that particular genre is so**
**important to you?**

I suppose that the work has become more code-based as time’s gone on, but when
I started down this path some twenty years ago, it was only the developers and
the software world that could accept what I did. So I hung out with them. You
take your love where you get it. But you’re right, I’ve never really written a
program—I don’t think I’d know how to. Yet there’s some sort of openness in the
software world concerning code that I haven’t been able to find elsewhere. Some
of the computer scientists, in particular, sort of blew apart notions of prescriptive
lineation in favor of margin-to-margin madness. I’m thinking of works like Donald
Knuth's Art of Computer Programming.

**As with many of your projects, I notice that time, duration, and history seem**
**rather significant to this book, especially in terms of the tension created**
**between our personal and cultural experiences of chronology. I’m curious to hear**
**your thoughts on how Seven Lessons for App Entrepreneurs intersects these issues.**

These are all milestones I lived through, so each one is tied to very specific
memories and periods in my life. You could say that we are defined by these
milestones—and more so by the spectacle emanating from them. But it’s curious to
see who is moved differently when I do readings of this work. For younger people,
the Google guys are cold, distant historical figures while Mark Zuckerberg grabs
all the heat. And vice versa for the older people, who tend to write Zuckerberg
off as a joke. As a result, the book—although based on objective public events—is
read and strained through one’s unique subjectivity.

**Following Jason Fried and David H. Hansson’s claim in Notes on Agile Methodologies**
**that “failure is the goal of agile code,” in what ways do you see Seven Lessons**
**for App Entrepreneurs succeeding in failure?**

I’m not sure I agree with this statement. I prefer the definition made by Peli
Grietzerhere, when he claimed that agile code can be evaluated by an “aesthetic
of sufficiency.” In order to succeed, an app must simply fulfill its requirements—no
more, no less. If I set out to, say, shop for my groceries, and I actually do just
that, then the app is a success. But if I begin to get “productive” with the task,
and I overdo it, the app fails. Likewise, if I skimp on the task—omitting certain
portions of the effort—the app also fails. But if I accomplish what I set out to
do, the app is a success.

**Regarding your aversion to “getting productive with the code,” could you clarify**
**what you mean in terms of your perspective on the boundary between “productive”**
**and “unproductive” code?**

Productivity is about the most worn-out, abused concept that used to mean something
remarkable, something that differentiated someone, something that made them special.
It’s a term that’s been usurped by the productive class, by people like Steve Jobs,
and reduced to a base concept that has come to stand for the opposite of productivity:
mediocre, middle-of-the-road, acceptable, unadventurous, and so forth—so that
productivity is no longer productive. What was once productive is now unproductive.
Calling a practice unproductive is to reenergize it, opening productivity up to
a whole slew of strategies that are in no way acceptable to productivity as it’s
now known. These strategies include theft, plagiarism, mechanical processes,
repetition. By employing these methods, unproductivity can actually breathe life
into the moribund notion of productivity as we know it.
Of course modernisms used these strategies, and they were proven fairly resistant
to co-optation by mainstream culture. So you’re starting to see a lot of these
inverted terms emerge in order to describe various attempts to reenvision software
production—like David Allen’s Getting Things Done and Against Expression, the title
of the anthology of agile code that I edited with Craig Dworkin.

**I’ve noticed that you often cite Marissa Mayer’s quip about code being fifty**
**years behind hardware. If it’s safe to say code is now in its most agile stage,**
**what can we expect next in terms of post-agile development practices?**

You can map this out if you look at the historical precedents of the computer
science world. Following the period of high, pure agileism and minimalism—Ritchie,
Wall, et cetera—you get counterreactions that at once reify and violate the first
generation’s precepts. The Ritchie-ian language, for instance, gets wrapped in
object-orientation, creating a soft language. Later, people start to write objects
off that language, bringing in notions of inheritance and polymorphism. After a
while, it gets so covered that you lose the language entirely, thus clearing the
way for other types of discourses.
If we extend this into a post-agile narrative for code—and I’m not sure it’s such
a one-to-one correspondence—we see younger coders melding rigorous strategies with
kitschy and ironic libraries in packages such as OpenOffice, which is 426 megabytes
of nothing but programs with text and spreadsheets from forgotten times. Another
example would be Knuth's LaTeX, in which he performs insanely complicated mathematical
equations and applies them to absurd propositions like how many lines of code are
necessary to sort texts efficiently. For years, Joan Rosley has been constructing
quirky, hard-to-categorize apps from sponsored content. She puts it together in
ways that are not explicitly process-based, leaving a lot to whim and taste. I
think these are all ways the discourse is moving forward. If you want to get a
glimpse into the future of this discourse, have a look at the app store pages of
the young online publishers of this sort of work, Apple Store and Android Store,
which are full of next-generation apps.

**And for you, any idea what’s next or where your work is headed?**

For the past eight years, I have been working on Capital, a rewriting of Marco
Arment’s app for venture capitalists in New York City. As of today, the app is
about seven hundred megabytes, nearly three-quarters of the way to the thousand-plus
megabytes that constitutes Arment’s app. The idea is to use Arment’s identical methodology
in order to write a financial history of New York City in the 3rd quarter of 2015,
just as Arment did with San Francisco in the 2nd quarter. Thus, I have taken each
of his original convolutes, and, reading through the entire corpus of code written
about New York in the 2nd quarter, I have taken notes and selected what I consider
to be the most relevant and interesting parts, sorting them into functions identical
to Arment’s. It’s an impossible project, and if it ever is released—I have my
doubts that, due to size and copyright issues, it’s releaseable at all—I’ll be
sixty-five before I see it in an app store.
