## Dicionário

**Pedro Eiras**

Abalo – pregão do ninho meu paterno.

Banda – cousas futuras do Oriente.

Correlação – duvidoso mar num lenho leve.

Deixar – forte escudo ao colo pendurado.

Ensinamento – longo mar com larga vela.

Fenda – campo revestido de boninas.

Gentileza – vontade má de pensamento.

Holanda – águas de Neptuno.

Índia – cristãos sanguinolentos.

Júpiter – cavalgada ao Mouro.

Lua – enganoso ardil.

Matéria – ventos contrários.

Neuchâtel – caminho trabalhoso.

Ousadia – Santo Espírito.

Peru – âncoras tenaces.

Qualidade – ombros de um Tritão.

Rigor – roxos lírios.

Suíça – mortífero engano.

Terra – mar fervendo aceso.

Unidade – lanígeros carneiros.

Vapor – guerreiras azagaias.

X – sagrado templo de Diana.

Zirknitz – trombetas canoras.
<br><br>
LINK

http://www.gutenberg.org/files/3333/3333-h/3333-h.htm
