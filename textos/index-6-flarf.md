## Sessão nº 8: Flarf

**14/04/2016**

<hr>

### Discussão

* Flarflist Collective: “FLARF: MAINSTREAM Poetry for a MAINSTREAM World”
* Gary Sullivan: “ORIGINAL FLARF”
* Gautam Naik: “Search for a New Poetics Yields This: 'Kitty Goes
Postal/Wants Pizza'”
* Jim Murdoch: “How to write flarf”
* Michael Magee: “The Flarf Files”

### Procedimentos

* Google sculpting / Flarf

Digite uma palavra ou combinação de palavras num motor de busca e registe os
resultados mais frívolos retirados de descrições de websites. Edite o material 
compilado.

* Spam Lit

Produza um texto a partir de mensagens spam.

### Textos produzidos

* [A galocha era o sonho de consumo de minha sobrinha neta.](http://laboratoriodepraticastextuaisexperimentais.com/?p=8-ms)
* [Flarf 1](http://laboratoriodepraticastextuaisexperimentais.com/?p=8-pe-1)
* [Flarf 2](http://laboratoriodepraticastextuaisexperimentais.com/?p=8-pe-2)
* [FLARF PENINSULAR](http://laboratoriodepraticastextuaisexperimentais.com/?p=8-bv)
* [rivotrilar](http://laboratoriodepraticastextuaisexperimentais.com/?p=8-gi)
* [RЕАDЕR Getting Over Garrett Delaney by Abby McDonald english how to store value iBooks](http://laboratoriodepraticastextuaisexperimentais.com/?p=8-aic)
* [Eu venho por este meio reclamar](http://laboratoriodepraticastextuaisexperimentais.com/?p=8-rl)

### Links e bibliografia

* Flarflist Collective. (2003-2013). [FLARF: MAINSTREAM Poetry for a
MAINSTREAM World](http://mainstreampoetry.blogspot.pt/).
* MURDOCH, Jim. (2011). [How to write flarf](http://jim-murdoch.blogspot.pt/2011/09/how-to-write-flarf.html)_. The Truth About Lies.
* MAGEE, Michael. (2003). “[The Flarf Files](http://epc.buffalo.edu/authors/bernstein/syllabi/readings/flarf.html)”.
* NAIK, Gautam. (2010). _[Search for a New Poetics Yields This: 'Kitty Goes
Postal/Wants Pizza](http://www.wsj.com/articles/SB10001424052748704912004575252223568314054)_. The Wall Street Journal.
* SULLIVAN, Gary. (2000). “[ORIGINAL FLARF](http://home.earthlink.net/~ululate/data/11-11-05.pdf)”.

### Material adicional

##### 1. Flarf

* CHICKSTON, Chickee. _[Three Poems](http://jacketmagazine.com/30/fl-chickston.html)_. Jacket 2, nº30.
* FISCHER, Shell. (2009). _[Can Flarf Ever Be Taken Seriously?](http://www.pw.org/content/can_flarf_ever_be_taken_seriously)_. Poets &
Writers.
* Flarflist Collective (2006). “[Flarf Festival 06](https://www.youtube.com/playlist?list=PLE20299DBC22BC3C3)”. Medicine Show Theater.
Nova Iorque, EUA.
* GABRIEL, Richard P. (2010). _[Defamiliarization: Flarf, Conceptual
Writing, and Using Flawed Software Tools as Creative Partners](https://www.dreamsongs.com/Files/Defamiliarization.pdf)_.
Knowledge Management & E-Learning: An International Journal, vol.2, nº1.
* GOTTLIEB, Michael. (2006). _[Googling Flarf](http://jacketmagazine.com/31/gottlieb-flarf.html)_. Jacket 2, nº31.
* HOY, Dan. (2006). _[The Virtual Dependency of the Post-Avant and the
Problematics of Flarf: What Happens when Poets Spend Too Much Time
Fucking Around on the Internet](http://jacketmagazine.com/29/hoy-flarf.html)_. Jacket 2, nº29.
* MAGEE, Michael (org). (2013). [Combo. nº12](https://media.sas.upenn.edu/jacket2/pdf/reissues/combo/J2_Reissues_Combo-12_Spring-2003.pdf). Pawtucket, EUA.
* SNYDER, Rick. (2006). _[The New Pandemonium: A Brief Overview of Flarf](http://jacketmagazine.com/31/snyder-flarf.html)_.
Jacket 2, nº31.
* SULLIVAN, Gary. (2000). _[Three poems](http://jacketmagazine.com/12/sullivan.html)_. Jacket 2, nº12.
* SULLIVAN, Gary. (2006). _[Jacket Flarf feature: Introduction](http://jacketmagazine.com/30/fl-intro.html)_. Jacket 2,
nº30.
* TARANKSY, Michelle (org). (2011). “[UNIT I: FLARF & ROUND](http://writing.upenn.edu/~taransky/unit%20i.pdf)”. ENGL 010 Writing From The Outside, University of Pennsylvania.
* TARZAN. (2006). _[Tarzan Workshop](http://jacketmagazine.com/30/fl-tarzan.html)_. Jacket 2, nº30.


##### 2. Flarf vs Conceptual Writing

* GARDNER, Drew. (2010). _WHY FLARF IS BETTER THAN CONCEPTUALISM_. TORRES,
Edwin. _[I’ll steal your poets like I stole your bike](http://www.poetryfoundation.org/harriet/2010/04/ill-steal-your-poets-like-i-stole-your-bike/)_. Harriet, Poetry
Foundation.
* GOLDSMITH, Kenneth (2009). _[Flarf is Dionysus. Conceptual Writing is
Apollo. An introduction to the 21st Century's most controversial poetry
movements](http://www.poetryfoundation.org/poetrymagazine/article/237176)_. Harriet, Poetry Foundation.
* PLACE, Vanessa, (2010). _Why Conceptualism is Better than Flarf_.
QUEIRAS, Syna. _[Flarf is a one-trick pony that thinks a unicorn is
another kind of horse](http://www.poetryfoundation.org/harriet/2010/04/flarf-is-a-one-trick-pony-that-thinks-a-unicorn-is-another-kind-of-horse/)_. Harriet, Poetry Foundation.
* PLACE, Vanessa. (2010). “[Why Conceptualism is Better than Flarf](https://www.youtube.com/watch?v=PBTPXbIVbTk)”. Flarf
& Conceptual Poetry Panel. AWP. Denver, USA.
