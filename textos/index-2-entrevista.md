## Sessão nº2: Entrevista

**25/02/2016**

<hr>

### Discussão

* Ted Berrigan: “An Interview with John Cage”
* Barbara Barg: “20 Questions”

### Procedimento

* Entrevista  

Produza uma falsa entrevista, utilizando material apropriado.

### Textos produzidos

* [Entrevista](http://laboratoriodepraticastextuaisexperimentais.com/?p=2-ms)
* [entrevista com ana bacalhau](http://laboratoriodepraticastextuaisexperimentais.com/?p=2-gi-1)
* [entrevista com caetano veloso em maio de 1968](http://laboratoriodepraticastextuaisexperimentais.com/?p=2-gi-2)
* [Um apoio de cerca de 700 euros por mês](http://laboratoriodepraticastextuaisexperimentais.com/?p=2-aic)
* [Woody and Charlie, clash of egos](http://laboratoriodepraticastextuaisexperimentais.com/?p=2-bv)
* [You Take Your App Where You Get It: An Interview with David Heinemeier Hansson](http://laboratoriodepraticastextuaisexperimentais.com/?p=2-rl)

### Links e bibliografia

* BARG, Barbara. (1984). _20 Questions_. “The L=A=N=G=U=A=G=E Book”,
BERNSTEIN, Charles, ANDREWS, Bruce (org). Southern Illinois University
Press. Carbondale e Edwardsville, EUA. 136-138: [1](http://eclipsearchive.org/projects/LANGUAGEn9/pictures/006.html), [2](http://eclipsearchive.org/projects/LANGUAGEn9/pictures/007.html).
* BERRIGAN, Ted. (2011). _[An Interview with John Cage](http://epc.buffalo.edu/authors/berrigan/cage.html)_. “Against
Expression, An Anthology of Conceptual Writing”. GOLDSMITH, Kenneth,
DWORKIN, Craig (org). Northwestern University Press. Evanston, EUA.
105-109.

### Material adicional

* [BARG, Barbara](http://barbarabarg.com/)
* BERRIGAN, Ted ([EPC Author Page](http://epc.buffalo.edu/authors/berrigan/index.html))
* BERRIGAN, Ted, KEROUAC, Jack, MCNAUGHTON, Duncan, SAROYAN, Aram. (1968).
_[Jack Kerouac interviewed by Ted Berrigan](http://www.theparisreview.org/interviews/4260/the-art-of-fiction-no-41-jack-kerouac)_. The Art of Fiction, nº41, The Paris Review, nº43. Nova Iorque, EUA.
* BERRIGAN, Ted, PADGETT, Ron, BRAINARD, Joe. (2012). “Bean Spasms”. 2ª ed: Granary Books. Nova Iorque, EUA.
* QUENEAU, Raymond & NOLL, Marcel, ARAGON, Louis & NOLL, Marcel, S.M. &
BRETON, André, S.M. & MORISE, Max, S.M & ARAGON, Louis, S.M. & NOLL,
Marcel, BRETON, André & PÉRET, Benjamin, ARTAUD, Antonin & BRETON,
André. (1928). ABIDOR, Mitchell (trad). _[Dialogue in 1928](https://www.marxists.org/history/france/surrealists/1928/dialogue.htm)_. La Révolution Surréaliste, nº11. Paris, França.
* WOLF, Reva. (2008, 2009). _[Mess and Message: Ted Berrigan’s Poetics of Appropriation](http://labos.ulg.ac.be/cipa/wp-content/uploads/sites/22/2015/07/89_wolf.pdf)_. Interval(le)s, II.2-III.1.
