## Sessões nº 6 e 7: OuLIPo

**31/03/2016** e **07/04/2016**

<hr>

### Discussão

* François Le Lionnais: “Lipo – First Manifesto” / “Second Manifesto”
* Jean Lescure: “Brief History of the Oulipo”
* Georges Perec: “History of the Lipogram”
* Harry Mathews: “Liminal Poem”

### Procedimentos

* Lista / Inventário 

Componha uma lista ou inventário, utilizando material apropriado.

* N + 7

Seleccione um texto e substitua cada substantivo pelo termo congénere que
ocupe a sétima posição posterior num dicionário.

* Lipograma 

Produza um texto, excluindo uma (ou várias) letra(s) do alfabeto.
Recorra, se necessário,  a material apropriado.

* Quimera 

Seleccione um texto (texto 1) e remova todos os substantivos, verbos,
adjetivos e advérbios. Selecione mais quatro textos (texto 2, texto 3,
texto 4 e texto 5). Substitua os substantivos eliminados do texto 1 por
substantivos retirados do texto 2. Substitua os verbos eliminados do
texto 1 por verbos retirados do texto 3. Substitua os adjetivos
eliminados do texto 1 por adjetivos retirados do texto 4. Substitua os
advérbios eliminados do texto 1 por advérbios retirados do texto 5.
Edite onde for necessário.

* Permutação Lescureana

Seleccione um texto. Altere a ordem dos substantivos do seguinte modo:
troque o primeiro pelo segundo e vice-versa, o terceiro pelo quarto,
etc.

* Homosintaxismo

Método de tradução no qual apenas a estrutura sintática é preservada.
Selecione um texto e, recorrendo a material apropriado, substitua todas
as palavras por termos congéneres. Utilize material apropriado.

* Homoconsonantismo

Seleccione um texto e elimine todas as vogais. Preserve as consoantes e
adicione o número de vogais que achar necessário, produzindo assim um
novo texto. Altere o espaçamento entre as letras onde for necessário.

* Variação (Homovocalismo)

Siga o mesmo procedimento com as consoantes.

* Bola de Neve

Constrangimento oulipiano no qual a primeira linha contém uma letra, a
segunda contém duas letras, etc.

* Variação (Bola de Neve a Derreter)

Siga o mesmo procedimento na ordem inversa.

* Fibonacci

Sequência na qual cada termo é a soma dos dois termos que o precedem (1,
2, 3, 5, 8, 13, 21, 34, etc.). Edite onde for necessário.

* Tradução Homofónica

Seleccione um texto escrito num idioma que não domine e *traduza* os fonemas.

### Textos produzidos

* [Ao minuto](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-aic-3)
* [Bola de neve & Bola de neve invertida](http://laboratoriodepraticastextuaisexperimentais.com/?p=7-ms)
* [Cá levá-la](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-rl-3)
* [Cantochão Negro](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-ms-2)
* [Com o dedo na tomada](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-gi-3)
* [Dois lances](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-bv-1)
* [Estás com o bitoque? Come uma neura](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-bv-5)
* [excêntrico](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-gi-2)
* [Fava rara](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-aic-1)
* [Homoconsonantismo](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-bv-4)
* [Lescurean Permutation](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-rl-2)
* [Lipograma](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-bv-3)
* [Lista de nomes permitidos em Portugal no ano corrente de 2016](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-gi-4)
* [N+7](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-bv-2)
* [Sem A, Infinito feito de ti](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-ms-1)
* [Twenty to one](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-rl-1)
* [Uma semana de rede](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-aic-2)
* [12. To write on papyrus with gondolas or simplicity.](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-aic-4)
* [22 coacções, coração](http://laboratoriodepraticastextuaisexperimentais.com/?p=6-gi-1)

### Links e bibliografia

* LESCURE, Jean. _Brief History of the Oulipo_. (1998). MOTTE JR., Warren F. (org, trad). “OULIPO – A Primer of Potential Literature”. 
2ª ed: Dalkey Archive Press. Mclean, EUA. 32-39.
* LIONNAIS, François Le. _Lipo – First Manifesto_. _Second Manifesto_. (1998). MOTTE JR., Warren F. (org, trad). “OULIPO – A Primer of 
Potential Literature”. 2ª ed: Dalkey Archive Press. Mclean, EUA. 26-31.
* MATHEWS, Harry. _Liminal Poem_. (1998). MOTTE JR., Warren F. (org, trad). “OULIPO – A Primer of Potential Literature”. 
2ª ed: Dalkey Archive Press. Mclean, EUA. 25.
* MOTTE JR., Warren F. (org, trad). (1998). _Table of Elementary Linguistic and Literary Operations_. “OULIPO – A Primer of 
Potential Literature”. 2ª ed: Dalkey Archive Press. Mclean, EUA. 44, 45.
* MOTTE JR., Warren F. (org, trad). (1998). _Glossary_. “OULIPO – A Primer of Potential Literature”. 2ª ed: Dalkey Archive 
Press. Mclean, EUA. 209-214.
* PEREC, Georges. _History of the Lipogram_. (1998). MOTTE JR., Warren F. (org, trad). “OULIPO – A Primer of Potential Literature”. 
2ª ed: Dalkey Archive Press. Mclean, EUA. 97-108.
* [ouvroir de littérature potentielle](http://oulipo.net/)

### Material adicional

* ANDREWS, Ian. (2012). “[Chance, Non-intention and Process](http://ian-andrews.org/texts/Chance_Non-intention_Process.pdf)”.
* CHRISTIAN, Peter. (2009). [The N+7 Machine](http://www.spoonbill.org/n+7/). "The Spoonbil Generator"
* MATHEWS, Harry, BROTCHIE, Alastair (org, trad). (1998). “[Oulipo Compendium](http://web.archive.org/web/20060508084921/http://www.oulipocompendium.com/html/home_frame.html)”. Atlas.
Londres, Reino Unido.
* MONFORT, Nick. (2014). “[Six Selections by the OuLIPo](http://nickm.com/classes/the_word_made_digital/2014_spring/12-oulipo-p.pdf)”.
* PEREC, George. (1973). _[L'art et la manière d'aborder son chef de
service pour lui demander une augmentation](http://www.persee.fr/doc/colan_0336-1500_1973_num_17_1_3984)_. Communication et langages,
vol.17, nº1. 41-56.
* QUENEAU, Raymond (1961). “Cent mille milliards de poèmes”. Gallimard.
Paris, França: [1](http://www.growndodo.com/wordplay/oulipo/10%5E14sonnets.html) 
[2](http://www.bevrowe.info/Queneau/QueneauRandom\_v4.html)
* QUENEAU, Raymond. (1973). _[Un conte à votre façon](http://www.infolipo.org/ambroise/cours/immediat/images/queneau_cavf.pdf)_. “Oulipo. La
littérature potentielle (Créations Re-créations Récréations)”.
Gallimard. Paris, França. 273-276.
* QUEYSANNE, Bernard, PEREC, George. (1974). “[Un homme qui dort](http://art-psy.com/PDF/Perec.pdf)”. França.
* The Found Poetry Review. (2014). "[OULIPOSTS](http://www.foundpoetryreview.com/oulipost/)".

