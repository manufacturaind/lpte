<br> <br>
##### Poetry has more to learn from graphic design, engineering, 

##### architecture, cartography, automotive design, or any other subject, 

##### than it does from poetry itself. 
<br> <br>
###### BEAULIEU, Derek. “Please, No More Poetry”.
