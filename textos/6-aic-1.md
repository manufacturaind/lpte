## Fava rara

**Ana Isabel Carvalho**

A porta, o gelo térreo, a dose sonsa,—aí há astros, aqui notas, aclaradas com paz; em cantis de que anis, netas de neve, ar ou anel no atrito. A mim sua cor, nação, sépia que não ousei... as granadas pedem glória.
Quem ama anis e a cor não usa, feno e seda já não arreia. Sem juta, soma peixes amorosos (como se gotas): de 13, dá fava rara, a 20 a dama raça nascia. O mês que há duas regas atiro na mesa:—de 21 dou e mereço 19 de borla. Se não ganhas só parado, anotas como signo alvo se o vale na cera põe maçã, despe a lufar cada adão.

<br><br>
Procedimento: Homoconsonatismo
<br><br>
### Link

[Da loucura em Portugal](https://www.gutenberg.org/files/34275/34275-h/34275-h.htm), XI Sinas {194}
