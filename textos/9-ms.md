## Sou uma criança grande… no fundo sou uma criança grande com medo dos fantasmas

**Maria Stella**

**Morri sem querer… como toda a gente:**

**Sr. Sargento: eu vou insultá-lo.**
<br><br>
Ah, o senhor não é um sargento!...

Oh, que malícia atroz! Oh, que sarcasmo diabólico!

Pois vem, não vem de sargento, vem de diabo! Ah, repito: o senhor não é um 
sargento!...

É ofensivo, é sim senhor! Prenda-me! Considere-se ofendido! E leve-me para a 
cadeia, já lhe disse!

Eu não me deito. Desobedeço-lhe! O senhor está a ver como eu lhe desobedeço? 
Desobedeço à autoridade… com acinte. Portanto leve-me preso, faça favor. Mas 
preso para  esquadra, ouviu? Preso para a cadeia. Não para o cemitério!

Eu vou chamar-lhe nomes feios. Mas o senhor zangue-se, por amor de Deus, e 
prenda-me, leve-me para a cadeia, meta-me num sítio onde eu veja o sol aos 
quadradinhos, mas onde veja o sol, hein, mesmo que seja aos quadradinhos! 
Faça-me este favor, seu…

O que mais há é seres humanos podres, sujos, lazeirentos, e habitam em cima, 
não debaixo da terra!
<br><br>
**Desisti do funeral**

Sou eu, confesso

Ah, és tu? Também estás cá? O que é que queres de mim? 

Fala.


Seja… entra. Talvez seja melhor fechar a porta, não?

Não fiquei satisfeito com a urna que a vossa casa me forneceu, como não ficaria 
satisfeito com qualquer outra urna que qualquer outra casa me fornecesse. 
Detesto urnas, pura e simplesmente.

São iguais às outras! são urnas e basta!

Sr. Agente funerário, vá fazer propaganda junto de outros fregueses, comigo 
perde o seu tempo!
<br><br>
**Morrer não basta. São indispensáveis certas formalidades…**

Ora, mas isso não é na minha classe social!

Ah pois falo! E olhe: você está com inveja!

Reduza-se à sua insignificância, mulherzinha!

Lembre-se de que sou rico, e você é pobre!

Lembre-se que os meus antepassados, além de ricos, eram condes, viscondes, 
marqueses, duques, super-duques, ou seja, por outras palavras: arqui-duques.
<br><br>
**Creia, pode crer, recuso-me, é verdade.**

Entendo. Mas agora entenda você: um cubículo de baile para mim não serve. 
Entendeu? Para dormir ou para bailar, exijo um salão, ou pelo menos uma sala… 
Entendeu?

Ora aí é que está, por isso mesmo!

Sim Senhor, por isso mesmo é que não me quero vestir correctamente: porque não 
quero ser enterrado. Impossível, não tenho calças. E ainda por cima não tenho 
casaco, nem tenho gravata…

Vou vestir uma gaita. Se eles me apanhassem de fato preto, consideravam-me 
pronto a ser enterrado.
<br><br>
**A alma? A alma não! A minha alma, mal eu morri, subiu ao céu, e nunca os cães
polícia a poderiam apanhar!...**
