## Entrevista

**Maria Stella**

E - O que nos mobilizou fundamentalmente a realizar esta entrevista foi a 
experiência de estranhamento familiar, uma espécie de espanto, mas também de 
empatia, despertada por ocasião da apresentação do seu romance O cheiro do ralo.
À medida que ia contando a respeito do protagonista, a maioria das pessoas foi
passando a detestá-lo e ao mesmo tempo a empatizar-se com ele. Algumas pessoas
mudavam de lugar, tentando aproximar-se de si, enquanto outras levantavam e 
deixavam a sala... E assim foi... O facto é que esta vivência teve a força de 
resistir dentro de nós até hoje, sustentando o desejo de marcar este encontro.
E, no entanto, são ideias tão simples as que apresenta: a beleza da 
simplicidade, a apologia da calma num mundo que corre a cada dia.

A - O que vou publicando sai-me naturalmente. Deixa-me com a sensação 
apaziguante de que é um trabalho genuíno. Permite-me um auto-domínio e uma 
inserção na espantosa realidade das coisas. Permite-me reflectir. Costumo dizer
que o meu trabalho é um mergulho. 

E - Sente-se, de alguma maneira, legítimo herdeiro dos velhos cronistas do 
Reino?

A - Acho que sim. O meu pai gostava muito de História e tinha uma propensão para
preferir a Idade Média por um certo Romantismo. Os escritores antigos falavam no
ípsilon. É um caminho que se divide em duas partes, nós vamos por um lado ou por
outro. É uma expressão simbólica, porque na realidade há muitos caminhos. Creio
que os que se dedicam à escrita da história se superestimem como grupo quando
supõem poder, por suas narrativas, cultivar ou estabelecer identidades 
políticas, o que, de certa maneira, é uma esperança idealista de muitos 
narrativistas. Mas há “o” caminho.

E – E esse caminho trouxe-o até aqui. À capacidade de inspirar emoções díspares 
e marcar a diferença.

A – Vejamos a coisa deste modo. Não tenho uma posição predeterminada sobre o 
que sou como viajante. Viajei muito sozinho ao fazer as minhas pesquisas e o 
que fui aprendendo foi a base para o meu trabalho.
 
E – Esse conhecimento do mundo traz um pouco de tudo. Questionar pode ser um
verdadeiro inferno e, no entanto, é a beleza que transborda para as obras e que
nos convoca e questiona. Não usa palavras caras, as imagens são banais, mas 
ainda assim com algo que as distingue do dia a dia.
 
A - Acho que, às vezes, as pessoas respeitam demasiado algumas coisas e por 
isso elas se repetem tanto. Acho que deveriam colocar assim: é isso que quero 
fazer e posso fazer desta forma e pronto. Para conhecer a verdade, é preciso, 
de início, colocar todos os nossos conhecimentos em dúvida. É necessário 
questionar tudo o que nos foi dado como verdadeiro e analisar, 
criteriosamente, se existe algo na realidade de que possamos ter plena 
certeza.
<br><br>
### LINKS

http://pepsic.bvsalud.org/scielo.php?script=sci_arttext&pid=S0101-31062008000200026

http://www.goncalocadilhe.com/entrevistas/201110_montepio.pdf

http://www.historiadahistoriografia.com.br/revista/article/view/884/585
