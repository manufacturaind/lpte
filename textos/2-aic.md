## Um apoio de cerca de 700 euros por mês[^1]

**Ana Isabel Carvalho**

**Qual é o desafio deste projecto?**

Esta escola tem como missão partilhar o seu potencial com as actividades que a 
comunidade desenvolve e que dele possam beneficiar, participando dessa forma no 
desenvolvimento regional. Mais tarde, poderão ser estudadas formas de 
comercializar este novo produto.[^2]
<br><br>
**Há 50 mil euros para quem descobrir. Por quanto tempo podes ignorar esta 
informação?**[^3]

A 1ª edição do projecto tem como objectivo desafiar estudantes portugueses a 
criarem um programa de voluntariado que catalise uma mudança positiva numa 
comunidade estrangeira com elevado impacto social, fazendo uso das capacidades 
e conhecimentos desenvolvidos durante o Ensino Superior, e financiando as suas 
ideias até dez mil euros.[^4]

Já a IBM tem 15.000 euros para melhor projecto em computação.[^5]
<br><br>
**Do que tens medo? Vontade de mudar só um pouco o dia-a-dia das pessoas?**[^6]

Em cima da mesa — além dos 25 mil euros para investimento — estão workshops de 
empreendedorismo criativo para os finalistas.[^7]

Deves também ser anexar um vídeo criativo (com a duração de um minuto) em que 
expliques a tua ideia. Podes desenvolver o teu programa de voluntariado 
individualmente ou convidar mais dois amigos para se juntarem a ti.[^4]
<br><br>
**Dez mil euros para projecto de português para acabar com dor na articulação da 
mandíbula?**

Cada um dos projectos pode receber até 10 mil euros em financiamento, bem como 
apoio especializado para tornar as suas ideias em planos viáveis e protótipos 
funcionais.[^3] Preenche um emoji-meter, uma espécie de termómetro para medir 
o medo que sentes antes, durante e depois da experiência.[^8]
<br><br>
**Porque é que ninguém quer este emprego de 240 mil euros?**

A incubadora visa facultar os meios e as infraestruturas para que os muitos 
agentes culturais em emergência e os que têm entusiasmo empreendedor encontrem a 
confiança e segurança na fase inicial dos seus projectos e carreiras, de forma a 
que se tornem sustentáveis.[^9]

Se as pessoas sentirem que o que estão a fazer é importante intrinsecamente e 
faz sentido para elas, em princípio, vão continuar, realçou o investigador, 
acrescentando que, na parte emocional, será testado o conceito mindfullness.[^10]
<br><br>
**Oferta de uma multiplicidade de iniciativas não pode ser confusa?**[^9]
**Por exemplo, crianças a ler para cães abandonados?**[^11]

A proposta passa por uma organização espacial que valoriza o espaço público, 
refletindo a ambição social do projecto.[^12] Antes, apropriando as duas 
vertentes, é revisitada como ferramenta conceptual. [^8]  No fundo, basta 
combater alguma inércia e ver um bocadinho menos de televisão.[^6]
<br><br>

### LINKS

[^1]: ["Start-up Voucher": apoio do Governo disponível em breve ↗](http://p3.publico.pt/actualidade/economia/20321/quotstart-voucherquot-apoio-do-governo-disponivel-em-breve) — P3  
[^2]: [Estudantes vão criar jóias com granito de Mondim de Basto ↗](http://p3.publico.pt/cultura/design/19764/estudantes-vao-criar-joias-com-granito-de-mondim-de-basto) — P3  
[^3]: [O que pode o design fazer pelos refugiados? Há 50 mil euros para quem descobrir ↗](http://p3.publico.pt/cultura/design/19759/o-que-pode-o-design-fazer-pelos-refugiados-ha-50-mil-euros-para-quem-descobrir) — P3  
[^4]: [Gap Year Portugal: dez mil euros para projectos que mudem o mundo ↗](http://p3.publico.pt/actualidade/educacao/19686/gap-year-portugal-dez-mil-euros-para-projectos-que-mudem-o-mundo) — P3  
[^5]: [IBM tem 15.000 euros para melhor projecto em computação ↗](http://p3.publico.pt/vicios/hightech/19600/ibm-tem-15000-euros-para-melhor-projecto-em-computacao) — P3  
[^6]: [Projecto Amélie quer mudar um pouco a nossa vida ↗](http://p3.publico.pt/cultura/palcos/2547/projecto-amelie-quer-mudar-um-pouco-nossa-vida) — P3  
[^7]: [Prémio das Indústrias Criativas já tem candidaturas abertas ↗](http://p3.publico.pt/actualidade/economia/19638/premio-das-industrias-criativas-ja-tem-candidaturas-abertas) — P3  
[^8]: [Michelle criou projecto para enfrentar 100 medos em 100 dias ↗](http://p3.publico.pt/actualidade/sociedade/17705/michelle-criou-projecto-para-enfrentar-100-medos-em-100-dias) — P3  
[^9]: [Santarém: vem aí uma Incubadora d'Artes aberta à "ousadia" ↗](http://p3.publico.pt/cultura/palcos/19510/santarem-vem-ai-uma-incubadora-d039artes-aberta-quotousadiaquot) — P3 
[^10]: [Projecto usa app para ajudar a manter peso de quem já o perdeu ↗](http://p3.publico.pt/actualidade/sociedade/18816/projecto-usa-app-para-ajudar-manter-peso-de-quem-ja-o-perdeu) — P3  
[^11]: [Projecto põe crianças a ler para cães abandonados ↗](http://p3.publico.pt/cultura/livros/19757/projecto-poe-criancas-ler-para-caes-abandonados) — P3  
[^12]: [Arquitectura: portugueses inspiram-se nos palheiros para projecto nas Ilhas Faroé ↗](http://p3.publico.pt/cultura/arquitectura/19591/arquitectura-portugueses-inspiram-se-nos-palheiros-para-projecto-nas-ilha) — P3  
