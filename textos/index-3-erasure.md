## SESSÃO Nº 3: ERASURE

**03/03/2016**

<hr>

### Discussão

* Robert Rauschenberg: “Erased de Kooning Drawing”
* Marcel Broodthaers: “Un coup de dés jamais n'abolira le hasard”
* Tom Phillips: “A Humument”
* Adam Smyth & Gill Partington: “Tom Phillips: An Interview”
* Derek Beaulieu: “Local Colour”
* Derek Beaulieu: "@erasingwarhol"
* Travis MacDonald: "A Brief History of Erasure Poetics"

### Procedimento

* Erasure

Seleccione um texto e elimine palavras até obter o resultado pretendido. 

### Textos produzidos

* Erasure de Mia Couto — [PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/3-ms.pdf)
* Don't kill lockdep — 
[PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/3-aic.pdf),
[HTML ↗](http://files.manufacturaindependente.org/lpte/sessao3/panic/)
* herberto sinistrado (16, 17, 30, 31, 32, 33) — [PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/3-gi.pdf)
* Sumários — [PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/3-rl.pdf)
* [The Religious Experience](http://laboratoriodepraticastextuaisexperimentais.com/?p=3-bv-1)

### Links e bibliografia

* BEAULIEU, Derek: (2011). “[Local Colour](http://eclipsearchive.org/projects/LOCAL/Local.pdf)”. Editions Eclipse.
* BEAULIEU, Derek. (2016). "[@erasingwarhol](https://twitter.com/erasingwarhol)".
* BROODTHAERS, Marcel. (1969). “Un coup de dés jamais n'abolira le
hasard”. Wide White Space Gallery. Antuérpia, Bélgica. Galerie Michael
Werner. Colónia, Alemanha.
  * [Un coup de Dés jamais n’abolira le Hasard — Mallarmé](http://www.coupdedes.com/)
  * [Un coup de Dés jamais n’abolira le Hasard — Broodthaers, HTML](http://www.moma.org/collection/works/146983)
  * [Un coup de dés jamais n'abolira le hasard — Broodthaers, PDF](http://monoskop.org/images/3/36/Broodthaers_Martin_Un_coup_de_des_jamais_n%27abolira_le_hasard.pdf)
* MACDONALD, Travis. (2009). _[A Brief History of Erasure Poetics](http://jacketmagazine.com/38/macdonald-erasure.shtml)_. Jacket 2, nº38.
* PHILLIPS, Tom. (2012). “[A Humument](http://www.tomphillips.co.uk/humument/slideshow/1-50)”, 5ª ed: Thames & Hudson. Londres, Reino Unido.
* PHILLIPS, Tom. (2015). “[Pages From A Humument](http://www.flowersgallery.com/exhibitions/view/tom-phillips-pages-from-a-humument)”. Flowers Gallery. Nova Iorque, EUA.
* RAUSCHENBERG, Robert. (1953). ”[Erased de Kooning Drawing](http://archv.sfmoma.org/explore/collection/artwork/25846)”. Colecção do SFMOMA. São Francisco, EUA.
* SMYTH, Adam, PARTINGTON, Gill. (2012). _[Tom Phillips: An Interview](http://www.lrb.co.uk/2012/10/04/tom-phillips/tom-phillips-an-interview)_. London Review of Books.

### Material adicional

* [BERVIN, Jen](http://www.jenbervin.com/)
* Wave Books. [Erasures](http://erasures.wavepoetry.com/).
* HICKMAN, Mary. (2014). _[Defaced/refaced books – The erasure practices of Jen Bervin and Mary Ruefle](http://jacket2.org/article/defacedrefaced-books)_. Jacket 2.
* MACDONALD, Travis, HOLMES, Janet, HARVEY, Matthea. (2011). “[Free Verse: Erasure Poetry Festival](https://www.youtube.com/watch?v=AWecWN5fP0U)”. Walker Art Center. Minneapolis, EUA.
* MALLARMÉ, Stéphane. (1914). “[Un coup de dés jamais n'abolira le hasard](http://writing.upenn.edu/library/Mallarme-Stephane_Coup_1914_spread.pdf)”. La Nouvelle Revue Française, Paris, França.
* MANSON, Peter. (2006). “[English in Mallarmé](http://ubuweb.com/ubu/unpub/Unpub_010_Manson_Mallarme.pdf)”. ubu editions.
* ROBERTS, Sarah. (2013). _[Erased de Kooning Drawing](http://archv.sfmoma.org/documents/research/EdeK_98.298/SFMOMA_RRP_Erased_de_Kooning_Drawing.pdf)_. Rauschenberg Research Project. SFMOMA. São Francisco, EUA.
* [RUEFLE, Mary](http://www.maryruefle.com/)
* VARLEY–WINTER, Rebecca. (2015). _[The Bark of the Language Forest: Peter
Manson’s ‘English in Mallarmé’](http://glasgowreviewofbooks.com/2015/08/13/the-bark-of-the-language-forest-peter-mansons-english-in-mallarme/)_. Glasgow Review of Books. Glasgow, Reino
Unido.
