## Erasure de Mia Couto, Murar o Medo

**Maria Stella**

O medo foi um dos meus primeiros mestres. Antes de ganhar confiança em 
celestiais criaturas, aprendi a temer monstros, fantasmas e demônios. Os anjos, 
quando chegaram, já era para me guardarem, os anjos atuavam como uma espécie de 
agentes de segurança privada das almas. Nem sempre os que me protegiam sabiam da 
diferença entre sentimento e realidade. Isso acontecia, por exemplo, **quando** me 
ensinavam a recear os desconhecidos. Na realidade, a maior parte da violência 
contra as **crianças** sempre foi praticada não por estranhos, mas por parentes e 
conhecidos. Os fantasmas que serviam na minha infância **reproduziam** esse velho 
engano de que estamos mais seguros em ambientes que reconhecemos. Os meus anjos 
da guarda tinham **a** ingenuidade de acreditar que eu estaria mais protegido apenas 
por não me **aventurar** para além da fronteira da minha língua, da minha cultura, 
do meu território. O medo foi, afinal, o mestre que mais me fez desaprender. 
Quando deixei a minha casa natal, uma invisível mão roubava-me a coragem de 
viver e a audácia de ser eu mesmo. **No horizonte** vislumbravam-se mais muros do 
que estradas. Nessa altura, **algo me sugeria** o seguinte: que há neste **mundo** mais 
medo de coisas más do que coisas más propriamente ditas.

No Moçambique colonial em que **nasci e cresci**, a narrativa do medo tinha um 
invejável casting internacional: os chineses que comiam crianças, os chamados 
terroristas que lutavam pela independência do país, e um ateu barbudo com um 
nome alemão. Esses fantasmas tiveram o fim de todos os fantasmas: morreram 
quando morreu o medo. Os chineses abriram restaurantes junto à nossa porta, os 
ditos terroristas são governantes respeitáveis e Karl Marx, o ateu barbudo, é um 
simpático avô que não deixou descendência. O preço dessa narrativa de terror 
foi, no entanto, trágico para o continente africano. Em nome da luta contra o 
comunismo cometeram-se as mais indizíveis barbaridades.

**Em** nome da **segurança** mundial foram colocados e conservados no Poder alguns dos 
ditadores mais sanguinários de toda a história**.** A mais grave **herança** dessa longa 
intervenção externa é a facilidade com que as elites africanas continuam a 
culpar os outros pelos seus próprios fracassos. A Guerra-Fria esfriou mas o 
maniqueísmo que a sustinha não desarmou, inventando rapidamente outras 
geografias do medo, a Oriente e a Ocidente.

Para responder às novas entidades demoníacas não bastam os seculares meios de 
governação. Precisamos **de investimento divino**, precisamos de intervenção de 
poderes que estão para além da força humana. O que era ideologia passou a ser 
crença, o que **era** política tornou-se religião, o que era religião passou a ser 
estratégia de poder. Para fabricar armas é preciso fabricar inimigos. Para 
produzir inimigos é imperioso sustentar fantasmas. A manutenção desse **alvoroço** 
requer um dispendioso aparato e um batalhão de especialistas que, em segredo, 
tomam decisões em nosso nome. Eis o que nos dizem: para superarmos as ameaças 
domésticas precisamos de mais polícia, mais prisões, mais segurança privada e 
menos privacidade.

Para enfrentarmos as ameaças globais precisamos de mais exércitos, mais serviços 
secretos e a suspensão temporária da nossa cidadania. Todos sabemos que o 
caminho verdadeiro tem que ser outro. Todos sabemos que esse outro caminho 
começaria pelo **desejo** de conhecermos melhor esses que, de um e do outro lado, 
aprendemos a chamar de “eles”. Aos adversários políticos e militares, juntam-se 
agora o clima, a demografia e as epidemias. O **sentimento** que se criou é o 
seguinte: a realidade é perigosa, a **natureza** é traiçoeira e a **humanidade** é 
imprevisível. Vivemos – como cidadãos e como **espécie – em permanente limiar de 
emergência**. Como em qualquer estado de sítio, as liberdades individuais devem 
ser contidas, a privacidade pode ser invadida e a racionalidade deve ser 
suspensa.

Todas estas restrições servem para que não sejam feitas perguntas incomodas 
como estas: porque motivo a crise financeira não atingiu a indústria de 
armamento? Porque motivo se gastou, apenas o ano passado, um trilhão e meio de 
dólares com armamento militar? Porque razão os que hoje tentam proteger os civis 
na Líbia, são exatamente os que mais armas venderam ao regime do coronel 
Kadaffi? Porque motivo se realizam mais seminários sobre segurança do que sobre 
justiça? Se queremos resolver (e não apenas discutir) a segurança mundial – 
teremos que enfrentar ameaças bem reais e urgentes. 
 
**Há** uma arma de destruição massiva que está sendo usada todos os dias, **em todo o 
mundo**, sem que seja preciso o pretexto da guerra. Essa arma chama-se fome. Em 
pleno século 21, um em cada seis seres humanos passa fome. O custo para superar 
a fome mundial seria uma fração muito pequena do que se gasta em armamento. A 
**fome** será, sem dúvida, a maior causa **de** insegurança do nosso **tempo**. Mencionarei 
ainda outra **silenciada** violência: em todo o mundo, uma em cada três mulheres foi 
ou será vítima de violência física ou sexual durante o seu tempo de vida. É 
verdade que sobre uma grande parte de nosso planeta pesa uma condenação 
antecipada pelo fato simples de serem mulheres. A nossa indignação, porém, é bem 
menor que o medo.
 
Sem darmos conta, fomos convertidos em soldados de um exército sem nome, e como 
militares sem farda deixamos de questionar. Deixamos de fazer perguntas e de 
discutir razões. As **questões** de ética são **esquecidas** porque está provada a 
barbaridade dos outros. E porque estamos em guerra, não temos que fazer prova de 
coerência, nem de ética e nem de legalidade. É sintomático que a única 
construção humana que pode ser vista do espaço seja uma muralha. A chamada 
Grande Muralha foi erguida para proteger a China das guerras e das invasões. A 
Muralha não evitou conflitos nem parou os invasores. Possivelmente, morreram 
mais chineses construindo a Muralha do que vítimas das invasões que realmente 
aconteceram. Diz-se que alguns dos trabalhadores que morreram foram emparedados 
na sua própria construção.  Esses **corpos convertidos em muro e pedra** são uma 
metáfora de quanto o medo nos pode aprisionar. **Há** muros que separam **nações**, há 
muros que dividem pobres e ricos. Mas não **há** hoje no **mundo**, muro que separe os 
que têm medo dos que não têm medo. **Sob as mesmas nuvens cinzentas** vivemos todos 
nós do sul e do norte, do ocidente e do oriente. Eduardo Galeano escreveu sobre 
o medo global: “Os que **trabalham** têm medo de perder o trabalho. **Os que não** 
trabalham têm medo de nunca encontrar trabalho. Quando não têm medo da fome, têm
medo da comida. Os civis têm medo dos militares, os militares têm medo da falta
de armas, as armas **têm medo** da falta de guerras. E, se calhar, acrescento 
agora eu, há quem tenha medo **que o medo acabe**.
