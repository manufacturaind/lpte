## SESSÃO Nº 10: Conclusão
**(28/04/2016)**

### Discussão

* Considerações finais sobre a formação.
* Projetos finais.
* Website do curso.

### Links e bibliografia

* BERNSTEIN, Charles (org). (2014). “[Experiments](http://www.writing.upenn.edu/bernstein/experiments.html)”.
* MAYER, Bernadette (org). (1984). _[Experiments](http://www.writing.upenn.edu/library/Mayer-Bernadette_Experiments.html)_. “The L=A=N=G=U=A=G=E Book”,
BERNSTEIN, Charles, ANDREWS, Bruce (org.). Southern Illinois University
Press. Carbondale e Edwardsville, EUA. 80-83. 
[1](http://eclipsearchive.org/projects/LANGUAGEn3/pictures/001.html), 
[2](http://eclipsearchive.org/projects/LANGUAGEn3/pictures/002.html), 
[3](http://eclipsearchive.org/projects/LANGUAGEn3/pictures/003.html), 
[4](http://eclipsearchive.org/projects/LANGUAGEn3/pictures/004.html).
