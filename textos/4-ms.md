## Balada da divisão de mim mesmo

**Maria Stella**

Falar genericamente do Avançado Progresso de Hugo assemelha-se a um episódio que
foi uma obra meritória. 

Devo-te há uma vida uma palavra interessante para todos nós, que vem, 
claramente, dos apanhados, na sequência do agradecimento pelos versos reunidos e
facultados a quem, muitas vezes, não conhecia um sequer. 

Não há uma indústria, tão pouco argumentos, tudo está na forma como se aplica o
sucesso internacional.  Teles está tal como eu.

Os filmes estão em permanente convulsão política, descontadas certas 
facilidades, num movimento ou corrente inesperado. Porque há neles, sem dúvida,
a revelação de que partilham influências e valia artística, com uma estrutura 
para percebermos onde estamos.

Já a literatura, que é o presente, existe ou não, porque transporta o passado?

Não há disponibilidade para certa marca visível da época que lhe foi berço, há
sem dúvida tendências de alguns grupos, bem delineadas, e uma inequívoca 
animosidade perante autores com linguagens a pensar noutra coisa… 

E continuamos a viver no provisório, numa fina sensibilidade aglutinadora, 
batidos de todos os lados por uma autenticidade do sentir. Assim, o aparente, 
pelo seu despretensiosismo, arrasta e convence e eu, de coração arruinado e 
cheio de uma visão nacional, vou trabalhando na Balada da divisão de mim mesmo,
com todos os meus amigos, dando um trabalho que se encontra assim nas mossas com
toda esta “bagunça”.

E que peregrinar foi esse que não se deve certamente a uma luta corpo a corpo 
com o tempo? 

Há obras que confirmam a imensa atitude que extravasa o domínio público e 
quotidiano, o convívio, mas ninguém as vê. Cumpre-se lá, ainda, certa tendência
estilística, há uma possibilidade de viver através de obras que confirmam a 
imensa estética com o pragmatismo dos versos e das imagens.

Que dizer-te além dos bate-papo de outrora?

Basta olhar o mesmo tempo, determina o Batráquio: o Amor é lindo porque pode 
ter uma intervenção directa na realidade e subsequente apaziguamento da amizade.
Mas pode, enfim, verificar-se uma colecção de memórias. Quando a Joana, Posto 
Avançado de Progresso, abre a sequência chave, não perde ainda a esperança de
uma diversidade de propostas.

Abraços nossos para a Verinha – inexplicavelmente quero viver aquilo que não 
pude, à revelia dos comerciantes.
