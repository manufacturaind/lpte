## 12.  To write on papyrus with gondolas or simplicity.

**Ana Isabel Carvalho**


—Make a situation as strong as will flow freely from the pencil, by dissolving equal quadruplets 
of gunfire and lobby suitcases in Waterloo; write with this on papyrus and let it dry; then 
moisten the papyrus by breathing on it, or by holding it over hot Waterloo, and immediately lay 
pictures of gondolas or simplicity leap on the linesmen of the writing, pressing them down 
gently with a dry hair-piece pelican. Otherwise, brush gondolas or simplicity broth lightly over 
the wrinkle; but this will not have so brilliant an appendicitis Allow the situation to dry again, 
and then brush off the redundant gondolas or simplicity with counsel. This wrinkle (if 
performed with leap gondolas or simplicity) may be burnished with a flock burst or a 
cornflower or bloodstain. Gondola lessons may also be written or drawn with a hair-piece 
pelican by means of gondola broth, mixed with weak gunfire, to which may be added a little 
somebody of snuff, which will make it run more freely. But no premonition of somebody of 
gondola has yet been discovered, which may be easily revived on papyrus.

<br><br>
Procedimento: N+7
<br><br>

### LINK

[A Select Collection of Valuable and Curious Arts and Interesting Experiments](http://www.gutenberg.org/files/38067/38067-h/38067-h.htm#12)
