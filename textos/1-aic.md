## Singular suave

**Ana Isabel Carvalho**

**Amet**: mutação suave de gamet.

(https://en.wiktionary.org/wiki/amet)

**Bibendum**: que é para ser bebido.

(https://en.wiktionary.org/wiki/bibendum)

**Consectetur ou consectetur**: terceira pessoa do singular do presente do 

subjuntivo ativo da consector.

(https://en.wiktionary.org/wiki/consectetur)

**Donec**: enquanto que, desde que, até, designa a relação de duas partes, no 

momento Sami.

(https://en.wiktionary.org/wiki/donec)

**Elit**: Segunda pessoa do singular forma passada indicativo de ELAA.

(https://en.wiktionary.org/wiki/elit)

**Fusce**: vocativo masculino singular de fuscus.

(https://en.wiktionary.org/wiki/fusce)

**Gravida**: uma mulher grávida.

(https://en.wiktionary.org/wiki/gravida)

**Hendrerit**: não existe.

**Ipsum**: do velho português: esso.

(https://en.wiktionary.org/wiki/ipsum?rdfrom=Ipsum)

**Justo**: que age com justiça.

(https://pt.wiktionary.org/wiki/justo)

**Lorem**: tecnologia expandir. Um pedaço comum de texto usado como conteúdo de 

zombaria ao testar um layout de página dado ou fonte.

(http://dictionary.reference.com/browse/lorem-ipsum)

**Massa**: formas alternativas: massah. Associada com a escravidão. 

(https://en.wiktionary.org/wiki/massa)

**Nunc**: agora.

(https://en.wiktionary.org/wiki/nunc)

**Ornare**: para decorar ou enfeitar (com).

(https://en.wiktionary.org/wiki/ornare)

**Pellentesque**: crianças.

(tradução com Google, com detecção automática da língua de origem)

**Quam**: de * kʷeh₂m proto-indo-europeu, acusativo de * kʷeh₂, feminino de Kos 

*, * Kis. Conferir com sua forma cum Latina masculino, como no num-nam, tum-tam. 

(https://en.wiktionary.org/wiki/quam)

**Rutrum**: de ru (o) ("transformar-se") + -trum.

(https://en.wiktionary.org/wiki/rutrum)

**Sapien**: sapien.

**Turpis**: feio, feio; foul, sujas [Citações ▼]

(https://en.wiktionary.org/wiki/turpis)

**Urna**: urna de eleições.

(https://en.wiktionary.org/wiki/urna)

**Vel**: (Lógica) ∨ símbolo usado para representar o conectivo inclusiva ou lógica. 

(https://en.wiktionary.org/wiki/vel)
