## Testes para formatação de texto 

### Alinhamento de texto

<p class="text-left">Parágrafos com texto alinhado à esquerda. Alinhamento à esquerda é o default do site por isso é uma indicação desnecessária ;-)</p>


<p class="text-right">Parágrafos com texto alinhado à direita.</p>


<p class="text-center">Parágrafos com texto alinhado ao centro.</p>


<p class="text-justify">
Parágrafos com texto justificado. Tem de ser comprido para se ver o efeito. 
Pommy ipsum unhand me sir munta alright geezer, Essex girls warts and all blimey. 
Could murder a pint a right royal knees up nose rag tally-ho black pudding therewith 
rather on his bill, that's ace apple and pears lass it's the bees knees knee high 
to a grasshopper i'll be a monkey's uncle. Bent as a nine bob note collywobbles 
it's the bees knees one would like rather roast beef pezzy little I'd reet fancy 
a cracking sausage roll, chaps blimey get away with ya that's ace corgi real 
ale terribly gallivanting around. Toad in the whole hadn't done it in donkey's 
years rambunctious what a doddle fancy a cuppa conked him one on the nose 
flip flops, completely crackers proper odds and sods bloody shambles.
</p>


Se for preciso usar **bold** ou _itálico_ dentro de uma parte HTML, as tags respectivas são:

<strong>Isto é texto bold</strong>

<em>Isto é texto itálico</em>

<p>Este parágrafo tem <strong>bold</strong>, <em>itálico</em> mas também tem <strong><em>bold itálico</em></strong>!
Mais de uma frase. <em>E uma frase inteira em itálico.</em></p>


### Finalmente os espaços brancos não são ignorados!


<pre><code>
 __
   | __
       | __
   *       | __
      *        | __    uma espécie de escada   __
         *                                 __ |
                                       __ |
                                   __ |

Aqui dentro não se podem usar tags HTML nem Markdown.
Não há bolds nem itálicos. Só espaços :-)

</code></pre>


### Um vídeo!

Se for Youtube, na página do vídeo clicar em "share", escolher a opção "embed" e copiar o código para aqui.
O vídeo abaixo vem [desta página](https://www.youtube.com/watch?v=Us79aL6CBGA).

<iframe width="420" height="315" src="https://www.youtube.com/embed/Us79aL6CBGA" frameborder="0" allowfullscreen></iframe>

<br><br>
Se for do Vimeo é o mesmo processo: share > embed e voilá:

<iframe src="https://player.vimeo.com/video/100592289?byline=0&portrait=0" width="640" height="470" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

