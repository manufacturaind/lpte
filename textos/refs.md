## 1. Dicionário

* Derek Beaulieu -- _[Please, No More Poetry](http://theamericanreader.com/bookcase/please-no-more-poetry-the-poetry-of-derek-beaulieu/)_
* Charles Bernstein -- _Thank you for saying thank you_: [vídeo](https://www.youtube.com/watch?v=auhINfzRcyY), [livro](https://books.google.pt/books?id=ZAkm1w2ctpoC&pg=PA7&lpg=PA7&dq=charles+bernstein+thank+you+for+saying+thank+you&source=bl&ots=kPcMJLpjcV&sig=QsqaVg4ENVp290veF0fE8QJ7Byg&hl=pt-PT&sa=X&ved=0ahUKEwjQ4LP8wf_KAhUFWhQKHX_1C-EQ6AEIUTAK#v=onepage&q=charles%20bernstein%20thank%20you%20for%20saying%20thank%20you&f=false)
* Charles Bernstein -- _What Makes a Poem a Poem_: [vídeo](https://www.youtube.com/watch?v=auhINfzRcyY), [transcrição](http://writing.upenn.edu/pennsound/x/Bernstein-What_Makes_a_Poem.html)

## 2. Entrevista

* Ted Berrigan -- _[An Interview with John Cage](http://epc.buffalo.edu/authors/berrigan/cage.html)_
* Barbara Barg -- _20 Questions_: [página 1](http://eclipsearchive.org/projects/LANGUAGEn9/pictures/006.html) e [página 2](http://eclipsearchive.org/projects/LANGUAGEn9/pictures/007.html)


* [The Art of Fiction: Jack Kerouac](http://www.theparisreview.org/interviews/4260/the-art-of-fiction-no-41-jack-kerouac)
* [Barbara Barg](http://barbarabarg.com/)
* http://labos.ulg.ac.be/cipa/wp-content/uploads/sites/22/2015/07/89_wolf.pdf
* https://www.marxists.org/history/france/surrealists/1928/dialogue.htm

## 3. _Erasure_

* _[Erasures](http://erasures.wavepoetry.com/)_ (Wave Books)
* Jen Bervin: [site](http://www.jenbervin.com/), [artigo sobre Jen Bervin e Mary Ruefle](http://jacket2.org/article/defacedrefaced-books)
* Mary Ruefle: [site](http://www.maryruefle.com/)
* Peter Manson: http://ubuweb.com/ubu/unpub/Unpub_010_Manson_Mallarme.pdf
* http://glasgowreviewofbooks.com/2015/08/13/the-bark-of-the-language-forest-peter-mansons-english-in-mallarme/


* Robert Rauschenberg -- _[Erased de Kooning Drawing](http://archv.sfmoma.org/documents/research/EdeK_98.298/SFMOMA_RRP_Erased_de_Kooning_Drawing.pdf)_
* Stéphane Mallarmé -- _[Un coup de dés jamais n'abolira le hasard](http://writing.upenn.edu/library/Mallarme-Stephane_Coup_1914_spread.pdf)_
* Stéphane Mallarmé / Livros de artista / Instalações / Poemas-objeto - http://www.lechasseurabstrait.com/revue_doc/pdf/mallarme_expo_toulouse.pdf
* Tom Phillips: “A Humument”

http://www.flowersgallery.com/exhibitions/view/tom-phillips-pages-from-a-humument

http://www.lrb.co.uk/2012/10/04/tom-phillips/tom-phillips-an-interview

 

Travis Macdonald / Janet Holmes / Matthea Harvey: “Free Verse: Erasure Poetry Festival”

https://www.youtube.com/watch?v=AWecWN5fP0U





## 4 e 5: _Cut-up_

* Anthony Balch & William S. Burroughs -- _[Towers Open Fire](http://www.ubu.com/film/burroughs_towers.html)_
* Christopher Land -- “Apomorphine Silence: Cutting-up Burroughs’ Theory of Language and Control”: http://www.ephemerajournal.org/sites/default/files/5-3land.pdf
* Oliver Harris -- _"Burroughs Is a Poet Too, Really": The Poetics of Minutes to Go_ - http://realitystudio.org/scholarship/burroughs-is-a-poet-too-really-the-poetics-of-minutes-to-go/_
* Marshall McLuhan -- “Notes on Burroughs (1964)”: http://realitystudio.org/criticism/notes-on-burroughs/
* William S. Burroughs & Brion Gysin -- The Third Mind (excerto: ver PDF anexado)



William S. Burroughs: “Towers Open Fire Screenplay”

http://realitystudio.org/bibliographic-bunker/towers-open-fire-screenplay/

 

 William S. Burroughs: “Technology of Writing” (ver PDF anexado)

  

  Dave Teeuwen: “The Soft Machines”

  http://realitystudio.org/criticism/the-soft-machines/

   

   Cut-Up Machines

   http://www.mundoblaineo.org/cut_up_machine.htm

   http://www.lazaruscorporation.co.uk/cutup/text-mixing-desk

   http://www.fetish23.com/words/cutup.html

   https://zoc.home.xs4all.nl/cut-up/ctpm.htm






## 6 e 7: Oulipo



Adicional:
Bernard Queysanne / George Perec: “Un homme qui dort”
http://www.imdb.com/title/tt0192718/
https://www.youtube.com/watch?v=AkNGpbO-krg
http://art-psy.com/PDF/Perec.pdf
 
George Perec: “L'art et la manière d'aborder son chef de service pour lui demander une augmentation”
http://www.persee.fr/doc/colan_0336-1500_1973_num_17_1_3984
 
Ian Andrews: “Chance, Non-intention and Process”
http://ian-andrews.org/texts/Chance_Non-intention_Process.pdf
 
Raymond Queneau: “Cent mille milliards de poèmes” http://www.growndodo.com/wordplay/oulipo/10%5E14sonnets.html
http://www.bevrowe.info/Queneau/QueneauRandom_v4.html 
 
Raymond Queneau: “Un conte à votre façon”
http://www.infolipo.org/ambroise/cours/immediat/images/queneau_cavf.pdf

Six Selections by the Oulipo
http://nickm.com/classes/the_word_made_digital/2014_spring/12-oulipo-p.pdf

Carta do escritor Guimarães Rosa composta sempre com palavras iniciadas pela letra C: http://www.elfikurten.com.br/2016/03/joao-guimaraes-rosa-carta-ao-consul.html

## 8. Flarf

Flarf / Flarf Collective
https://en.wikipedia.org/wiki/Flarf_poetry
http://jim-murdoch.blogspot.pt/2011/09/how-to-write-flarf.html
https://www.poets.org/poetsorg/text/brief-guide-flarf-poetry
http://www.wsj.com/articles/SB10001424052748704912004575252223568314054
http://mainstreampoetry.blogspot.pt/
http://epc.buffalo.edu/authors/bernstein/syllabi/readings/flarf.html
http://home.earthlink.net/~ululate/data/11-11-05.pdf
 

Notas adicionais:

Chickee Chickston; “Three Poems”
http://jacketmagazine.com/30/fl-chickston.html
 
Combo, nº12, Primavera 2003 (ed. Michael Magee)
https://media.sas.upenn.edu/jacket2/pdf/reissues/combo/J2_Reissues_Combo-12_Spring-2003.pdf
 
Dan Hoy: “The Virtual Dependency of the Post-Avant and the Problematics of Flarf: What Happens when Poets Spend Too Much Time Fucking Around on the Internet”
http://jacketmagazine.com/29/hoy-flarf.html
 
Flarf Collective: “Flarf Festival 06”
https://www.youtube.com/playlist?list=PLE20299DBC22BC3C3
 
Flarf Collective: “FLARF & ROUND”
http://writing.upenn.edu/~taransky/unit%20i.pdf
 
Gary Sullivan: “Three poems”
http://jacketmagazine.com/12/sullivan.html
 
Gary Sullivan: “Jacket Flarf feature: Introduction”
http://jacketmagazine.com/30/fl-intro.html
 
Michael Gottlieb: “Googling Flarf”
http://jacketmagazine.com/31/gottlieb-flarf.html
 
Richard P. Gabriel: “Defamiliarization: Flarf, Conceptual Writing, and Using Flawed Software Tools as Creative Partners”
https://www.dreamsongs.com/Files/Defamiliarization.pdf
 
Rick Snyder: “The New Pandemonium: A Brief Overview of Flarf”
http://jacketmagazine.com/31/snyder-flarf.html
 
Shell Fischer: “Can Flarf Ever Be Taken Seriously?”
http://www.pw.org/content/can_flarf_ever_be_taken_seriously
 
Tarzan: “Tarzan Workshop”
http://jacketmagazine.com/30/fl-tarzan.html
 
 
2. Flarf vs Conceptual Writing

Drew Gardner: “WHY FLARF IS BETTER THAN CONCEPTUALISM”
http://www.poetryfoundation.org/harriet/2010/04/ill-steal-your-poets-like-i-stole-your-bike/
 
Kenneth Goldsmith: “Flarf is Dionysus. Conceptual Writing is Apollo. An introduction to the 21st Century's most controversial poetry movements.”
http://www.poetryfoundation.org/poetrymagazine/article/237176
 
Kenneth Goldsmith: “Flarf vs. Conceptual Writing”
http://www.poetryfoundation.org/harriet/2008/12/flarf-vs-conceptual-writing/
http://www.poetryfoundation.org/harriet/2008/12/flarf-vs-conceptual-writing-2/
 
Vanessa Place: “Why Conceptualism is Better than Flarf”
https://www.youtube.com/watch?v=PBTPXbIVbTk
 
Vanessa Place: “Notes on why Conceptualism is Better than Flarf”
http://www.poetryfoundation.org/harriet/2010/04/flarf-is-a-one-trick-pony-that-thinks-a-unicorn-is-another-kind-of-horse/ 


## 9. Conceptual Writing


Christian Bök: CBCVictoria. The Xenotext - a poem in bacteria.
https://soundcloud.com/cbcvictoria/putting-a-poem-in-bacteria-experimental-poet-chritian-bok-is-doing-it

Christian Bök and Kenneth Goldsmith: “Conceptual Poetics”
https://www.youtube.com/watch?v=D667k70AHoM

Kenneth Goldsmith (EPC Author Page)
http://wings.buffalo.edu/epc/authors/goldsmith/   

Kenneth Goldsmith (UbuWeb Contemporary)
http://www.ubu.com/contemp/goldsmith/index.html

Kenneth Goldsmith: “Poetry will be made by all”
http://quietube7.com/v.php/http://www.youtube.com/watch?v=Otb6yNmyFz8

Kenneth Goldsmith: “Reads poetry at White House Poetry Night”
https://www.youtube.com/watch?v=hMSvrIPhA4Y

Kenneth Goldsmith: “Reads Traffic Reports to Obama”
https://www.youtube.com/watch?v=eho6boLMUg0

Kenneth Goldsmith: “Revenge of the Text”
http://jackhenriefisher.com/readings/RevengeOfTheText.pdf

Open Letter: A Canadian Journal of Writing and Theory
Twelfth Series, Number 7, Fall 2005
“Kenneth Goldsmith and Conceptual Poetics”
Edited by Barbara Cole & Lori Emerson
http://www.ubu.com/papers/kg_ol.html
http://epc.buffalo.edu/authors/goldsmith/Goldsmith-Open_Letter.pdf
 
Craig Dworkin
http://epc.buffalo.edu/library/Dworkin_Craig_A-Handbook-of-%20Protocols-for-Literary-Listening.pdf
 
Sol Lewitt
http://www.corner-college.com/udb/cproVozeFxParagraphs_on_Conceptual_Art._Sol_leWitt.pdf
http://viola.informatik.uni-bremen.de/typo/fileadmin/media/lernen/LeWittSentencesConceptual.pdf
 
Lawrence Weiner: Statements
http://www.ubu.com/papers/weiner_statements.html
http://radicalart.info/concept/weiner/
http://www.jeffreythompson.org/downloads/LawrenceWeiner-Statements.pdf

vale acompanhar este caso envolvendo borges: http://www.gazetadopovo.com.br/caderno-g/literatura/escritor-engorda-um-conto-de-borges-bqtixf125v9bzohxelyx489yi
