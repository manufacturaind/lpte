## Sessões nº4 e 5: Cut-up

**10/03/2016** e **17/03/2016**

<hr>

### Discussão

* William S. Burroughs: “The Cut-Up Method of Brion Gysin” / Brion Gysin: “Cut-Ups Self-Explained” / Brion Gysin: “Minutes to Go”
* Antony Balch & William S. Burroughs: “Towers Open Fire”
* Christopher Land: “Apomorphine Silence: Cutting-up Burroughs’ Theory of Language and Control”
* Oliver Harris: “Burroughs Is a Poet Too, Really: The Poetics of Minutes to Go”
* Marshall McLuhan: “Notes on Burroughs”

### Procedimentos

* Cut-up (1) 

Seleccione um texto e divida-o na vertical em quatro partes iguais. Reordene-as, 
transcreva-as e edite-as. Repita o procedimento as vezes que forem necessárias.

* Fold-in

Seleccione vários textos e imprima-os. Dobre-os a meio e reorganize as
metades, formando novas secções. Transcreva-as e edite-as.

* Cut-up (2): Intersecção

Siga as instruções do procedimento Cut-Up (1) e adicione material
retirado de vários media. Forme novas secções, transcreva-as e edite-as.

### Textos produzidos

* [A origem dos meus sonhos. José Tavares](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-aic)
* [AVES E PORCO IMBATÍVEIS ](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-rl)
* [Balada da divisão de mim mesmo](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-ms)
* [Cut-up 1](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-bv-1)
* [Cut-up 2](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-bv-2)
* [E sei, quando a manhã nasce, que conseguirei ainda levar o cérebro a cabo](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-gi-1)
* [Eia, humanista!](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-pe)
* [snapshot scherzo: a república federativa brasileira (16/03/2016)](http://laboratoriodepraticastextuaisexperimentais.com/?p=4-gi-2)

### Links e bibliografia

* BALCH, Antony & BURROUGHS, William S. (1963). “[Towers Open Fire](http://www.ubu.com/film/burroughs_towers.html)”. Reino Unido.
* BURROUGHS, William S., GYSIN, Brion. (1978). _The Cut-Up Method of Brion
Gysin, Cut-Ups Self-Explained, Minutes to Go_. "The Third Mind". The
Viking Press. Nova Iorque, EUA. 29-40.
* HARRIS, Oliver. (2005). _[’Burroughs Is a Poet Too, Really’: The Poetics
of Minutes to Go](http://realitystudio.org/scholarship/burroughs-is-a-poet-too-really-the-poetics-of-minutes-to-go/)_. The Edinburgh Review, nº144. Edimburgo, Reino Unido.
* LAND, Christopher. (2005). _[Apomorphine Silence: Cutting-up Burroughs’
Theory of Language and Control](http://www.ephemerajournal.org/sites/default/files/5-3land.pdf)_. ephemera, vol.5, nº3.
* MCLUHAN, Marshall. (1964). _[Notes on Burroughs](http://realitystudio.org/criticism/notes-on-burroughs)_. RealityStudio.

### Material adicional

* BURROUGHS, William S. (2013). _Technology of Writing_. "The Adding
Machine". Grove Press. Nova Iorque, EUA. 39-45.
* BURROUGHS, William S. (1963). _[Towers Open Fire Screenplay](http://realitystudio.org/bibliographic-bunker/towers-open-fire-screenplay/)_. Film, nº37.
* BURROUGHS, William S., BEILES, Sinclair, GYSIN, Brion, CORSO, Gregory.
(1960). “[Minutes to Go](http://realitystudio.org/texts/the-poetry-of-william-s-burroughs/cut-up-poems-from-minutes-to-go)”. Two Cities Editions. Paris, França.
* TEEUWEN, Dave. (2009). _[The Soft Machines](http://realitystudio.org/criticism/the-soft-machines/)_. RealityStudio.
* WATSON, Paul. (2014). _[The Text Mixing Desk](http://www.lazaruscorporation.co.uk/cutup/text-mixing-desk)_. “The Lazarus Corporation”.
