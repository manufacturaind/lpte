## E SEI, QUANDO A MANHÃ NASCE, QUE CONSEGUIREI AINDA LEVAR O CÉREBRO A CABO

**Gabriel Innocentini**

É por isso que mal vem a prima como a versão Ali Babá de uma fantasia: a roupa, 
o apartamento, os horários são um mostruário completo do repertório cúmplice do 
que consideramos como telhados-florestas, fachadas multicores, e encolhe os dias 
de colunas convexas. Os ângulos superiores eram dinheiro frio e mesquinho, acentuado 
pelas colunas lápis-azuli hostis. Defrontámo-lo vestidos, coroados com bolas douradas 
criando distraídos artifícios que se tornam a chaminé-minarete, a cúpula bulbosa – 
e o mal e a primavera se aproximam. Um dia destes, de repente o ar de cebola de 
20,24m x 17,8m, em esmalte da cidade torna-se-me insuportável em 1100m2, resplandece 
a 100 metros a natureza e chama por mim como se a sua mensagem de felicidade viesse 
através das montanhas. O vento murmura por encomenda da cidade. Os presidentes com 
seus chilreios, ao longe ouve-se o génio criador de Hungria no paraíso perdido. 
Já que não posso ser urbano e permanecer sem as provas tangíveis vou aos arbustos 
para colher bagas em Hundertwasser. Está profundamente cheio de mirtilhos pós-modernos.

Um verso envolvente, carnal, com o seu dualismo de extensão por toda a vida a 
penetrar as mulheres nas quais cavou entre o corpo e a alma. A sexualidade das 
mulheres da vida se tornou propícia a esta espécie de pedaço de carne e, no seu 
acto sexual clássico, nas finadas secreções, isso é então o útero. O seu sexo 
absoluto, porque ele recusou o cordão umbilical. O objectivo emotivo é, ao mesmo 
tempo, fazer-se absorver, para ela, a corrente físico-química dos fenómenos nos 
quais por sua vez tem a mãe em lugar das leis estruturais. Porquê? A mulher é 
arredada do seu estado e do impulso do apetite, que a ele regressa regularmente 
na percepção óptica dos adultos.

No entanto, também somos o mesmo sentido que em contacto com essa beatitude, aliás, 
nunca é, como pensa um delicado casulo, a formação de perfumes, de adornos, o 
processo vital, enraizado na existência da mulher-mulher e na sua estrutura.

Logo que uma menina-estômago de modo puramente maternal propaga a dependência do 
sistema nervoso pelos olhos, se pusesse os sapatos de salto alto, sem a sugestão 
de comer meninos que também se sentem feito a manducação real. A sexualidade não 
se define a partir de Descartes mas nas pulsões sexuais ou não, e jamais abre cada 
genuíno movimento para as primeiras emoções, o querer. Ele, com toda a fisiologia 
de um travesti, ou com a fisiologia de uma feminista, tem as partes do corpo orgânico, 
no plano morfológico, de nossas mães.

O fato de o verde de há alguns anos vir a esta parte é vital para a arquitectura. 
Na pintura de casas enuncia o feminino, com todos os seus elementos: argila, barro, 
cal, carvão vegetal mesmo que o reboco seja desigual em nossa jornada de trabalho. 
Também exprimiu o voto conclusivo: «Quando deixarmos a natureza confortável, mesmo 
unissexo, livres do obrigatório cinto dos prazeres, voltaremos a viver». Este texto 
de 1981 traduz as preocupações dominantes de tratar do rosto e do corpo da Löwengasse, 
cuja maqueta fora apresentada em total beleza, a remexer na primeira pedra colocada 
em 1983. Sente-se moralmente de lingerie…cultivar esta linha recta que lançara há 
30 anos. O sentido é cultivar a deusa que há e que uma vez realizada adquirirá um 
valor exemplar, ilustrando o sagrado que é a casa, o lar, o tangível das suas 
convicções arquitectónicas. O moralista no espaço intra-ulterino dá razão ao teórico 
da arquitectura que se torna o seu médico no dia-a-dia. De facto, nos últimos 15 anos, 
continuando a sua actuação no segredo dos nossos quartos, Hundertwasser concebe 
mais de 50 maravilhosos umbigos de estruturas e de redesign de fachadas. Uma 
exposição que deixa a todos mais maravilhados…
Uma vez que os elementos estruturais batem na mesma porta várias noites seguidas 
e a cada instante, então é de esperar, sem queixar-me deixo-me guiar pelo acaso, 
fio-me no psicológico, um poderoso excesso, mesmo em vários quartos a fluir e 
mesmo sem estímulos externos que nunca se encontram, não se conhecem na abolição 
da consciência vígil e dentro deste palácio só me conhecem além disso, como 
mostrei noutro lugar onde nos encontramos, sejamos todos absolutamente originários. 
O meu labirinto permite-me fazer consciência vígil, como em uma cadeia profunda. 
Mas há muitos que no meio do ritmo dos estados de sono se perdem nesse universo nocturno. 
Aprendo a parecer ser, também em maior medida por meio dos gestos do seu corpo, 
onde descubro o sabor animal, o genuíno órgão da morte, com tão grande falta e sem 
centralização. Por detrás de cada um deles está a actividade cerebral. Sabemos, 
porém, quem dará prazer, a quem eu darei prazer: ao cão ou ao cavalo, e sei, 
quando a manhã nasce, que conseguirei ainda levar o cérebro a cabo. Penso que um 
dia enfim amarei.

Por todo o lado, nos propõem a «biologia a partir de dentro» e devemos segui-la 
como se fosse a única. Esta, sem dúvida, avança no conhecimento da maquilhagem. 
Claro que não na característica formal do organismo, porque uma fantasia nunca 
deve esquecer os parceiros, nem os últimos elementos celulares do cinema ou dos 
livros. É o desejo de concretizar tudo, o corpo inteiro passando pelas células, 
pelo sexo em grupo, por exemplo, só é, a todo o instante, algo que envolve 
dinamicamente actores virtuais. É o processo vital; e sabemos que, na evolução 
das actividades em grupo, sejam elas nitidamente distintas das funções demasiado 
propícias a se fazer ditas, eu procuro no homem as formas estáticas, fantasiar enriquece. 

Ontologicamente, rigorosamente, irei sorver com a minha boca. A diferença é apenas 
que não terei mais medo, UM plano fenomenal, de rigor, que dará ao real o ritmo 
do seu decurso. Os processos das minhas fantasias. Aliás, nem todos os processos 
são apenas dois e sexuais. O simples facto não-mecânico segundo a natureza estimula 
a interacção das suas funções. Actualmente encorajam-nos ao fenómeno vital. Há uma 
fantasia grosseira, que é a «biologia a partir de fora», anda em moda, na última linha 
de roupas, indo ao que há de mais contagiante nos organismos, nas formas vivas reflectidas. 
A ideia dos tecidos e dos órgãos me seduz na condição de ser sustentada e reformada, 
sou muito refractária às «funções estruturantes», sejam quais forem (infelizmente, 
as funções orgânicas propriamente ressaltam a nossa mediocridade da situação físico-química 
– nem se fosse um deus vivo).
Sem dúvida, sentia-se a queratina tradicional da imortalidade. Ela só podia dar-se, 
tal como a construção do carácter está à venda não interessam as características 
corporais. Há qualquer outra. Nua, assimilada, incomparavelmente toda a «espécie» 
feminina – provocadora, sim, tratá-la como uma putazita. Também eu, à noite, em 
comparação a ele era muito menor. Me sentia bem por ser puramente funcional. Mas 
à beira do Atlântico passeio pelo correlato fisiológico ao natural…e numa abordagem 
psicológica, sinto-me sempre mal vestida, o que me causa «sublimação» [Sublimierung]. 
Nua, sinto as funções sensório-motoras, mas não no organismo animal, embora seja da natureza. 
Já não é a repartição de energia entre o cérebro e o constatar que estou viva. 
