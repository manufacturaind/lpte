## SESSÃO Nº 9: Conceptual Writing

**21/04/2016**

<hr>

### Discussão

* Christian Bök: “Chapter E from ‘Eunoia’” (an interactive digital version
by Brian Kim Stefans)
* Christian Bök: “The Xenotext"
* Kenneth Goldsmith: “I Love Speech”
* Kenneth Goldsmith: “UNCREATIVE WRITING IN THE CLASSROOM”
* Kenneth Goldsmith: “Why Conceptual Writing? Why Now?”
* Kenneth Goldsmith: “Fidget”
* Kenneth Goldsmith: “Soliloquy”
* Kenneth Goldsmith: “Traffic”
* Richard Kostelanetz: “WHAT WAS ‘CONCEPTUAL WRITING’?”
* Vanessa Place: “Gone with the Wind”
* Vanessa Place: “Statement of Facts”

### Procedimento

* Texto Conceptual

Produza um texto a partir de processos de apropriação textual e deslocação do 
contexto.

### Textos produzidos

* [ESPECIAL COIMBRA](http://laboratoriodepraticastextuaisexperimentais.com/?p=9-aic-1)
* [Found Mag Articles](http://laboratoriodepraticastextuaisexperimentais.com/?p=9-bv-2)
* [Lulagrafia](http://laboratoriodepraticastextuaisexperimentais.com/textos/9-gi-1.pdf) (PDF)
* [Morna](http://laboratoriodepraticastextuaisexperimentais.com/?p=9-bv-1)
* [os 2 primeiros parágrados d’os maias, de eça de queirós, de acordo com uma reforma ortográfica proposta no congresso brasileiro](http://laboratoriodepraticastextuaisexperimentais.com/?p=9-gi-2)
* [Sou uma criança grande… no fundo sou uma criança grande com medo dos fantasmas](http://laboratoriodepraticastextuaisexperimentais.com/?p=9-ms)
* [Comentadora de jornal](http://laboratoriodepraticastextuaisexperimentais.com/?p=9-rl)

### Links e bibliografia

* BÖK, Christian. (2015). “[The Xenotext (Book 1)”. Coach House Books.
Toronto, Canadá. 
* BÖK, Christian. (2001). “Eunoia". Coach House Books. Toronto, Canadá.
* BÖK, Christian. (2002). “[Chapter E from ‘Eunoia’](http://www.ubu.com/contemp/bok/eunoia_final.html)” (an interactive digital
version by Brian Kim Stefans).
* GOLDSMITH, Kenneth. (2000). “Fidget”. Coach House Books. Toronto, Canadá.
* GOLDSMITH, Kenneth. (2001). “Soliloquy”. Granary Books. Nova Iorque,
EUA. [1 — HTML](http://www.epc.buffalo.edu/authors/goldsmith/soliloquy/index.html)
[2 — PDF](http://wings.buffalo.edu/epc/authors/goldsmith/soliloquy_book.pdf) 
* GOLDSMITH, Kenneth. (2006). “[Traffic](http://eclipsearchive.org/projects/TRAFFIC/traffic.html)”. Editions Eclipse.
* GOLDSMITH, Kenneth. (2007). “Traffic”. Make Now. Los Angeles, EUA. 
* GOLDSMITH, Kenneth. (2007). _[I Love Speech](http://www.poetryfoundation.org/features/articles/detail/68773)_. Harriet, Poetry Foundation.
* GOLDSMITH, Kenneth, DWORKIN, Craig (org). (2011). _Why Conceptual
Writing? Why Now?_ “Against Expression”. Northwestern University Press. 
Evanston, EUA.
* GOLDSMITH, Kenneth. (2011). _Uncreative Writing in the Classroom: A Disorientation_.
“Uncreative Writing: Managing Language in the Digital Age”. Columbia
University Press. Nova Iorque, EUA.
* KOSTELANETZ, Richard. (2014). _[WHAT WAS ‘CONCEPTUAL WRITING’?](http://www.utsanga.it/index.php/kostelanetz-what-was-conceptual-writing/)_ Utsanga,
nº2.
* PLACE, Vanessa. (2010). “[Statement of Facts](http://epc.buffalo.edu/authors/place/Place_GWTWselect.pdf)”. Blanc Press. Los Angeles,
EUA.
* PLACE, Vanessa. (2011). “[Gone with the Wind](http://epc.buffalo.edu/authors/place/Unpub_042_Place.pdf)”. ood Press. Rio de Janeiro,
Brasil.


### Material adicional

* BÖK, Christian. (2016). “[The Xenotext - a poem in bacteria](https://soundcloud.com/cbcvictoria/putting-a-poem-in-bacteria-experimental-poet-chritian-bok-is-doing-it)”.
CBCVictoria, Canadá.
* BÖK, Christian, GOLDSMITH, Kenneth. (2014). “[Conceptual Poetics](https://www.youtube.com/watch?v=D667k70AHoM)”. Museu
de Arte Contemporânea de Houston. Houston, EUA.
* COLE, Barbara, EMERSON, Lori (org). (2005). Open Letter: A Canadian
Journal of Writing and Theory, série 12, nº7. 
[1 — HTML](http://www.ubu.com/papers/kg_ol.html), [2 — PDF](http://epc.buffalo.edu/authors/goldsmith/Goldsmith-Open_Letter.pdf)
* DWORKIN, Craig. (2012). “A Handbook of Protocols for Literary
Listening”. Arika’s *A survey is a process of listening* program.
Whitney Biennial. Nova Iorque, EUA. [PDF](http://epc.buffalo.edu/library/Dworkin_Craig_A-Handbook-of-%20Protocols-for-Literary-Listening.pdf)
* GOLDSMITH, Kenneth ([EPC Author Page](http://wings.buffalo.edu/epc/authors/goldsmith/))
* GOLDSMITH, Kenneth ([UbuWeb Contemporary](http://www.ubu.com/contemp/goldsmith/index.html))
*GOLDSMITH, Kenneth. (2011). _[REVENGE OF THE TEXT](http://jackhenriefisher.com/readings/RevengeOfTheText.pdf)_. “Uncreative Writing:
Managing Language in the Digital Age”. Columbia University Press. Nova
Iorque, EUA.
* GOLDSMITH, Kenneth. (2011). “[Reads poetry at White House Poetry Night](https://www.youtube.com/watch?v=hMSvrIPhA4Y)”.
Washington D.C., EUA.
* GOLDSMITH, Kenneth: (2014). “[Reads Traffic Reports to Obama](https://www.youtube.com/watch?v=eho6boLMUg0)”. Nova
Iorque, EUA.
* GOLDSMITH, Kenneth. (2015). “[Poetry will be made by all](http://quietube7.com/v.php/http://www.youtube.com/watch?v=Otb6yNmyFz8)”. 89plus After
Babel Stockholm Sessions, Museu de Arte Moderna de Estocolmo. Estocolmo,
Suécia.
* LEWITT, Sol. (1967). *[Paragraphs on Conceptual Art](http://www.corner-college.com/udb/cproVozeFxParagraphs_on_Conceptual_Art._Sol_leWitt.pdf)*. Artforum, Verão 1967. 
Nova Iorque, EUA.
* WEINER, Lawrence. (1968). “Statements”. Seth Siegelaub. Nova Iorque,
EUA. [1 — HTML](http://www.ubu.com/papers/weiner_statements.html), 
[2 — PDF](http://www.jeffreythompson.org/downloads/LawrenceWeiner-Statements.pdf), 
[3 — HTML](http://radicalart.info/concept/weiner/)
