## Projectos Finais

### Kernel Comments

##### Ana Isabel Carvalho

_Os ficheiros de código do Kernel incluem comentários dos autores para documentar 
o que cada parte faz. Como são ficheiros centrais do sistema operativo, são editados 
regularmente e os comentários são atualizados. Cada linha preta é uma linha de código 
e a sua largura corresponde à largura do texto / código que lá estava. O texto refere-se 
a comentários de autores / programadores sobre cada parte do código._

Um dos princípios base do software livre é o acesso ao código. Ana Isabel Carvalho 
selecionou ficheiros gerais do Kernel do Linux e aplicou-lhes procedimentos de Erasure. 
Escolheu especificamente o Kernel por ter sido uma parte essencial para a criação 
do primeiro sistema operativo livre. 

Kernel Comments é um exercício metalinguístico que nos remete para um jogo de 
sombras entre script, estructuras gráficas e sociolectos. 

[PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/Kernel%20Comments%20-%201.pdf), 
[HTML ↗](http://files.manufacturaindependente.org/lpte/reading-kernel/)

----

### Pontos Finais

##### Soares Vieira

Pontos Finais, da autoria de Soares Vieira, é um e-chapbook composto por 
11 textos onde é explorado o conceito de ponto final. Trata-se de um diário mínimo, 
onde a metalinguagem e operações formais como o acróstico, a apropriação, o inventário, 
a tradução homofónica, o googlismo e a Erasure Poetics se cruzam com a experiência 
fragmentária do quotidiano.

[PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/pontos-finais.pdf)


----

### brasil, superfuckingcrazycut

##### Gabriel Innocentini

O que há em comum entre a série televisiva The Americans e a Operação Lava Jato? Gabriel Innocentini concebeu uma operação combinatória / aleatória onde a tensão entre texto e imagem nos transporta para um labirinto de procedimentos e referências que nos remete para as noções baudrillardianas de simulação e simulacro:

- transcrições de The Americans;
- marcação temporal realizada por meio da cronologia da Operação Lava Jato;
- lista das operações da Lava Jato;
- listas de votos de congressistas;
- transcriação (ou tradução livre) de poemas canónicos atribuídos a falsos autores (William Carlos Williams vs. Michel Temer);
- adulteração de "documentos" (Carta da dona Lúcia);
- montagem (vídeos de José Dirceu e Roberto Jefferson a debater ao longo do tempo – apropriação de um título de um romance de Joseph Conrad);
- inclusão de notícias ou citações sem nenhum comentário;
- substituição simples de termos ("sua" por "lula" numa canção de Adriana Calcanhotto).

[Link ↗](http://koizo.org/paginas-instaveis/)

(Concepção do website: Manufactura Independente)

----

### contraponto

##### Maria Stella

Produzido durante a 1ª edição do Laboratório de Práticas Textuais Experimentais, 
contraponto, de Maria Stella, é uma breve coleção de poemas onde um duelo entre 
o campo e a cidade é esgrimido em pares de poemas, através de procedimentos de 
Erasure e antinomia. 

[PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/contraponto.pdf)

----

### Sumários

##### Ricardo Lafuente

Em Sumários, Ricardo Lafuente, numa abordagem que podemos classificar de brutalista, 
explorou a dimensão gráfica / arquitectónica de composições vazias, apropriando-se, 
para esse efeito, de compêndios burocráticos. Utilizando procedimentos de Erasure 
sobre páginas do Diário da República e relatórios do Instituto Nacional de 
Estatística, produziu uma série de mapas pragmáticos de organização. Sumários 
sinaliza os edifícios onde mora o texto, estructuras funcionais despojadas – as 
ruínas da cultura impressa.

[PDF](http://laboratoriodepraticastextuaisexperimentais.com/textos/sumarios.pdf)
